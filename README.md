# Bachelor thesis #

## Title:  Preprocessing of virus/host MS/MS data for de novo sequencing using machine learning to improve identification rate.

Start : 01.06.2017

Due to: 10.08.2017

Author: Felix Hartkopf

Thesis advisor: Dr. Thilo Muth

1st supervisor: Dr. Bernhard Y. Renard

2nd supervisor: Dr. Knut Reinert 

## Scope
In MS-based proteomics, the method of de novo sequencing has recently become more accurate because of latest algorithmic evelopments and increased resolution of the instrumentation. However, the de novo sequencing approach strongly depends on the quality of the input MS/MS spectra, because noise peaks and incomplete fragmentation significantly reduce the overall accuracy. Most de novo sequencing algorithms in shotgun proteomics use relatively simple methods which are often based on top-peak intensity preprocessing. This shortcoming will be tackled in this work, so that the objective is to develop a more sophisticated preprocessing method to improve the correct identification yield of peptides predicted by de novo sequencing algorithms. As scaffold, a machine learning approach (random forest or neural networks) will be implemented with a selection (or generation) of confident features, which enables to reduce noise in MS/MS spectra and to simplify the task of de novo sequencing for better performance and more accurate results. The spectra will be processed independently (i.e. all peaks within a spectrum) and as a part of a whole data set to account for reoccurring peptide fragment signatures. A special focus is directed to virus/host proteomics, because, in spite of higher resolution and in general more accurate instruments, the higher mutation rate of viruses results in fewer correctly identified peptides and proteins. A detailed evaluation of the newly developed preprocessing tool showing the effect on the performance of de novo sequencing will be conducted using HCD and CID experimental and simulated spectra with a solid ground truth. This bachelor thesis will be supervised by Dr. Bernhard Y. Renard and advised by Dr. Thilo Muth (MF1, Robert Koch Institute).  

  

---

* Literature research 
	+ Machine learning
		+ Supervised Neural Networks (SNN) (Cleveland, 2013)
    + De novo sequencing algorithms
    	+ PepNovo (pub?)
    	+ Novor
    	+ ms2preproc (pub?)
    	+ MSNovo
    	+ Pilot
    	+ pNovo
    + MS/MS preprocessing
    	+ What is common Renard et al.,2009
    		- no preprocessing
    		- only top peaks
    	+ Spectral quality scoring (Renard et al.,2009) I
    		- feature or clustering based
    	+ Precursor preprocessing (Renard et al.,2009) II
    		- identifying charge state
    	+ MS/MS spectra preprocessing (Renard et al.,2009) III
    		- Correcting monoisotopic masses of the precursor
    		- identifying charge state
    + Fragmentation
    	+ y-,b-,a-,z-,x-,c-ions
    	+ Ammonia, water and other neutral loss ions
    	+ Isotopes 
    	+ Noise (unidentifiable ions)
    + Already existing prprocessing tools
    	+ Mascot Distiller (Renard et al.,2009) II III
    	+ MS Cleaner (Renard et al.,2009) II
    	+ DTASuperCharge (Renard et al.,2009) II III
    	+ OMSSA (Renard et al.,2009) III
    		- -+ 25 m/z window and top 6 peaks
    	+ InSpect (Renard et al.,2009) III
    		- -+ 27 m/z (charge 2) or -+ 27 m/z (charge 3) window and top one or two peaks
    	+ mzStar - cut off based (Renard et al.,2009) III
    	+ wiff2data - cut off based (Renard et al.,2009) III
    	+ MaxQuant, slide window (100 m/z) top peakes (Renard et al.,2009) II III
    + What is the default preprocessing of tool X?
    	+ PepNovo: Sliding window 56 Da threshold and three highest peaks (Cleveland, 2013)
    	+ ms2preproc:  Sliding window threshold (Cleveland, 2013)
    	+ MSNovo: Sliding window 100 Da threshold and six highest peaks (Cleveland, 2013)
    	+ PILOT: 125 most intense peaks (Cleveland, 2013)
    	+ pNovo: 100 most intense peaks (Cleveland, 2013)
* Analysis of experimental HCD/CID spectra
	+ Classification of ions (Y,B,...) (Cleveland, 2013)
* Feature selection for a machine learning approach for identification prognosis
	+ Mass windows(lowest mass to highest mass)
	+ Number of spectra
	+ Mean intensity
	+ Similarity with predicted spectrum (MS2PIP)
* Analysis of experimental (CPXV,RVFV and ADV11), synthetic (http://www.proteometools.org/) and simulated data 
	+ Identification  if of experimental data through TATAR-databse (MSGF+)
* Peak detection
* Filtering of neutral loss ions (ammonia, water,...)
* Transformation to singly (+1) charged spectras to reduce complexity
* Noise Reduction through Supervised Neural Network (SNN) ion detection(https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3907776/). Successor publication? 
	+ Less peaks are leading to better and feaster results, Cleveland(2013)
	+ Neural Network Tutorial (http://www.theprojectspot.com/tutorial-post/introduction-to-artificial-neural-networks-part-2-learning/8)
* Validation of preprocessing with Novor, maybe TATAR-DeNovo without Modifications/Mutations

### Features
* BA, Schmerling, 2012
	+ Rank
	+ Intensity/base peak intensity
	+ Intensity/total ion current
	+ Local rank
	+ Intensity/highest peak in interval
	+ Intensity/total ion current in interval
	+ Mass-to-charge ratio
	+ Loss ions (H2O, NH3, ...)
	+ complementary peaks
	+ Adjacent ions
	+ Distance to neighbors/average Distance to neighbors in S
	+ Peak m/z - precursor m/z
	+ precursor charge
* Cleveland, 2012
	+ Relative cleavage
	+ Strong peak 
	+ Isotopologue
	+ Principal iso.
	+ y^2-ion
	+ Radom hypothesis
	+


<p align="center">
  <img src="https://gitlab.com/HartkopfF/bachelor_thesis/raw/65bf650f306f284e309f4866ee8cf7306b697ecf/concept.png" width="1400"/>
</p>