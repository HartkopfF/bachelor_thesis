'''
Created on 30.07.2017

@author: fhartkopf
'''
import matplotlib.pyplot as plt
import Input2 as Input  # @UnresolvedImport
import numpy as np

def countIons(file):
    print("Reading...")

    peaks = Input.readTrainingsSet(file)

    a = 0
    b = 0
    c = 0
    x = 0
    y = 0
    z = 0
    noise = 0
    other = 0
    peakCount = 0
    print("Counting...")


    for peak in peaks:
        ionClass = peak.ionClass
        peakCount+=1
        if(ionClass== "a"):
            a+=1
        elif(ionClass== "b"):
            b+=1
        elif(ionClass== "c"):
            c+=1    
        elif(ionClass== "x"):
            x+=1    
        elif(ionClass== "y"):
            y+=1    
        elif(ionClass== "z"):
            z+=1    
        elif(ionClass== "noise"):
            noise+=1 
        else:
            other+=1
        
    print(a,b,c,x,y,z,noise,other)
    print(a/peakCount,b/peakCount,c/peakCount,x/peakCount,y/peakCount,z/peakCount,noise/peakCount,other/peakCount)
    print(a+b+c+x+y+z+noise+other,peakCount)
    
    results = [a,b,c,x,y,z,noise,other,peakCount]
    return results

def plotHist(): 
    ###########################
    # synthetic/MSGFPlus plot #
    ###########################

    N = 2
    unprocessedSynthetic_mean = np.mean( [0.45744539913825616, 0.3577211774289191])
    unprocessedMFGSPlus_mean = np.mean([ 0.5336925703848696, 0.6658582714589142,  0.5462408032453616, 0.5448622939195708])
    unprocessed_means = (unprocessedSynthetic_mean,unprocessedMFGSPlus_mean)
    ind = np.arange(N)  # the x locations for the groups
    width = 0.35       # the width of the bars
    
    fig, ax = plt.subplots(figsize=(20, 10))
    rects1 = ax.bar(ind, unprocessed_means, width, color=color0)
    
    processedSynthetic_mean = np.mean([0.46512621189744324, 0.33924140292604227])
    processedMFGSPlus_mean = np.mean([0.5141938701393493, 0.6698862455465642, 0.5356241103133482, 0.5231483752718747])
    processed_means = (processedSynthetic_mean,processedMFGSPlus_mean)
    
    
    rects2 = ax.bar(ind + width, processed_means, width, color=color1)
    
    # add some text for labels, title and axes ticks
    ax.set_ylabel('Recall')
    ax.set_title('Novor results recall of synthetic and MSGFPlus as ground truth ')
    ax.set_xticks(ind + width / 2+0.175)
    ax.set_xticklabels(('synthetic', 'MSGFPlus'))
    
    ax.legend((rects1[0], rects2[0]), ('unprocessed', 'processed'), loc=2, prop={'size': 20})
    fig.savefig('recall_syntheticMSGFPlus.png')
    plt.show()


########
# Main #
########

color0 = "red"
color1 = "forestgreen"

font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 22}

plt.rc('font', **font)


# file ="../../res/spectra/UPS1/UPS1_12500amol_R1.mgf_training.mgf"
file ="../../res/spectra/pyro/Pyro_Velos.mgf_training.mgf"

countTraining = countIons(file)