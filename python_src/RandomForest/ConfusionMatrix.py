'''
Created on 12.07.2017

@author: HartkopfF
'''

import Input
import os
import pandas
import numpy
from sklearn.ensemble import RandomForestClassifier


if __name__ == '__main__':
    pass




# Change directory
os.chdir("../..")
cwd = os.getcwd()
print(cwd)
fraction = 0.5
print(fraction)

# Import peaks
file = "res/spectra/pyro/Pyro_Velos.mgf_training.mgf"
peaks = Input.readTrainingsSet(file)
print(len(peaks), "peaks were imported.")


####################
# Data Preparation #
####################

# Create matrix out of all peaks
peaksMatrix = [peaks[0].getHeader()]
for peak in peaks:
    row = peak.getAsRow()
    peaksMatrix = numpy.vstack([peaksMatrix, row])
    steps = (len(peaksMatrix)/len(peaks))*100
    print("%.2f" % steps, "%")

 
# Create dataframe
df = pandas.DataFrame(peaksMatrix[1:], columns=peaksMatrix[0])
print(df[:10])
features = df.columns[2:]
print(features)
ionClassFact, ionClassDict = pandas.factorize(df['ionClass'])
print(ionClassDict)
df['ionClassFact'] = ionClassFact
print(df['ionClass'])
print(ionClassFact)
 
  
#################
# Random Forest #
#################
# Fitting
clf = RandomForestClassifier(n_jobs=-1)
df['is_train'] = numpy.random.uniform(0, 1, len(df)) <= fraction
train, predict = df[df['is_train']==True], df[df['is_train']==False]
 
# Balance training data
train = Input.balanceValidation(train)
 
fit = clf.fit(train[features], train['ionClassFact'])
print(fit)

# Create classification dictionary and apply it
ionClassDict = dict(zip(range(7), ionClassDict))
print(ionClassDict)
preds = clf.predict(predict[features])
preds = numpy.vectorize(ionClassDict.get)(preds)

print(preds)
 
 
print(clf.predict(predict[features]))
print(clf.predict_proba(predict[features]))
print(preds)
   
    
print(predict['ionClass'].head())
   
# Create confusion matrix
print(pandas.crosstab(predict['ionClass'], preds, rownames=['Actual Ion Class'], colnames=['Predicted Ion Class']))
    
# View a list of the features and their importance scores
print(list(zip(df[features], clf.feature_importances_)))

