'''
Created on 16.08.2017

@author: hartkopff

'''
"""
========
Barchart
========

A bar plot with errorbars and height labels on individual bars
"""
import numpy as np
import matplotlib.pyplot as plt

color0 = "red"
color1 = "forestgreen"

font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 22}

plt.rc('font', **font)
 
# ##############################
# # processed/unprocessed plot #
# ##############################
# N = 6
# unprocessed_means = (0.45744539913825616, 0.3577211774289191, 0.5336925703848696, 0.6658582714589142,  0.5462408032453616, 0.5448622939195708)
#  
# ind = np.arange(N)  # the x locations for the groups
# width = 0.35       # the width of the bars
#  
# fig, ax = plt.subplots(figsize=(20, 10))
# rects1 = ax.bar(ind, unprocessed_means, width, color=color0)
#  
# processed_means = (0.4438809021697915, 0.34215867255945376, 0.48599387067955385, 0.6765719982239186, 0.5034126914249781, 0.5213536132902675)
# rects2 = ax.bar(ind + width, processed_means, width, color=color1)
#  
# # add some text for labels, title and axes ticks
# ax.set_ylabel('Recall')
# ax.set_title('Novor results recall of unmodified and modified spectra')
# ax.set_xticks(ind + width / 2+0.175)
# ax.set_xticklabels(('synthetic\nCID', 'synthetic\nHCD', 'pyrococcus\nfuriosus\nHCD', 'human\nCID', 'yeast\nCID', 'UPS1 yeast\nCID'))
#  
# ax.legend((rects1[0], rects2[0]), ('unprocessed', 'processed'), loc=2, prop={'size': 20})
# fig.savefig('recall_unprocessedprocessed.png')
#  
# #plt.show()
#  
# ################
# # HCD/CID plot #
# ################
#  
# N = 2
# unprocessedHCD_mean = np.mean( [0.3577211774289191, 0.5336925703848696])
# unprocessedCID_mean = np.mean([0.45744539913825616, 0.6658582714589142,  0.5462408032453616, 0.5448622939195708])
# unprocessed_means = (unprocessedHCD_mean,unprocessedCID_mean)
# ind = np.arange(N)  # the x locations for the groups
# width = 0.35       # the width of the bars
#  
# fig, ax = plt.subplots(figsize=(20, 10))
# rects1 = ax.bar(ind, unprocessed_means, width, color=color0)
#  
# processedHCD_mean = np.mean([0.34215867255945376, 0.48599387067955385])
# processedCID_mean = np.mean([0.4438809021697915, 0.6765719982239186, 0.5034126914249781, 0.5213536132902675])
# processed_means = (processedHCD_mean,processedCID_mean)
#  
#  
# rects2 = ax.bar(ind + width, processed_means, width, color=color1)
#  
# # add some text for labels, title and axes ticks
# ax.set_ylabel('Recall')
# ax.set_title('Novor results recall of HCD and CID spectra')
# ax.set_xticks(ind + width / 2+0.175)
# ax.set_xticklabels(('HCD', 'CID'))
#  
# ax.legend((rects1[0], rects2[0]), ('unprocessed', 'processed'), loc=2, prop={'size': 20})
# fig.savefig('recall_HCDCID.png')
#  
# #plt.show()
#  
# ###########################
# # synthetic/MSGFPlus plot #
# ###########################
#  
# N = 2
# unprocessedSynthetic_mean = np.mean( [0.45744539913825616, 0.3577211774289191])
# unprocessedMFGSPlus_mean = np.mean([ 0.5336925703848696, 0.6658582714589142,  0.5462408032453616, 0.5448622939195708])
# unprocessed_means = (unprocessedSynthetic_mean,unprocessedMFGSPlus_mean)
# ind = np.arange(N)  # the x locations for the groups
# width = 0.35       # the width of the bars
#  
# fig, ax = plt.subplots(figsize=(20, 10))
# rects1 = ax.bar(ind, unprocessed_means, width, color=color0)
#  
# processedSynthetic_mean = np.mean([0.46512621189744324, 0.33924140292604227])
# processedMFGSPlus_mean = np.mean([0.48599387067955385, 0.6765719982239186, 0.5034126914249781, 0.5213536132902675])
# processed_means = (processedSynthetic_mean,processedMFGSPlus_mean)
#  
#  
# rects2 = ax.bar(ind + width, processed_means, width, color=color1)
#  
# # add some text for labels, title and axes ticks
# ax.set_ylabel('Recall')
# ax.set_title('Novor results recall of synthetic and MSGFPlus as ground truth ')
# ax.set_xticks(ind + width / 2+0.175)
# ax.set_xticklabels(('synthetic', 'MSGFPlus'))
#  
# ax.legend((rects1[0], rects2[0]), ('unprocessed', 'processed'), loc=2, prop={'size': 20})
# fig.savefig('recall_syntheticMSGFPlus.png')
# plt.show()


#######################
# data reduction plot #
#######################
    
N = 6
data = [0.237848315,0.296980946,0.337785007,0.272715624,0.235301404,0.362440559]
ind = np.arange(N)  # the x locations for the groups
width = 0.6       # the width of the bars
  
fig, ax = plt.subplots(figsize=(20, 10))
rects1 = ax.bar(ind, data, width, color=color0)  # @UnusedVariable
         
# add some text for labels, title and axes ticks
ax.set_ylabel('data reduction (%)')
ax.set_title("Data Size Reduction")
ax.set_xticks(ind + width / 2)
ax.set_xticklabels(('synthetic\nCID', 'synthetic\nHCD', 'pyrococcus\nfuriosus\nHCD', 'human\nCID', 'yeast\nCID', 'UPS1 yeast\nCID'))
   
#ax.legend((rects1[0]), ('unprocessed'), loc=2, prop={'size': 20})
fig.savefig('data_reduction.png')
plt.show()

######################
# noise deleted plot #
######################
    
N = 6
data = [0.243958356,0.301433997,0.352979812,0.306155886,0.2668993,0.391793954]
ind = np.arange(N)  # the x locations for the groups
width = 0.6       # the width of the bars
  
fig, ax = plt.subplots(figsize=(20, 10))
rects1 = ax.bar(ind, data, width, color=color0)  # @UnusedVariable
         
# add some text for labels, title and axes ticks
ax.set_ylabel('removed noise (%)')
ax.set_title("Removed Noise Peaks")
ax.set_xticks(ind + width / 2)
ax.set_xticklabels(('synthetic\nCID', 'synthetic\nHCD', 'pyrococcus\nfuriosus\nHCD', 'human\nCID', 'yeast\nCID', 'UPS1 yeast\nCID'))
   
#ax.legend((rects1[0]), ('unprocessed'), loc=2, prop={'size': 20})
fig.savefig('noise_deleted.png')
plt.show()