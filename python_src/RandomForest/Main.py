'''
Created on 30.06.2017

@author: hartkopff
'''

import Input
import os
import pandas
import numpy
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn import datasets
from sklearn import svm
from sklearn.model_selection import cross_val_score

if __name__ == '__main__':
    pass

# Change directory
os.chdir("../..")
cwd = os.getcwd()
print(cwd)


# Import peaks
file = "res/spectra/UPS1/UPS1_12500amol_R1.mgf_traning.mgf"
tbcFile = "res/spectra/UPS1/UPS1_12500amol_R1.mgf_tbc.mgf"
peaks = Input.readTrainingsSet(file)
print(len(peaks), "peaks were imported.")


####################
# Data Preparation #
####################
# Create matrix out of all peaks
peaksMatrix = [peaks[0].getHeader()]
for peak in peaks[:1000]:
    row = peak.getAsRow()
    peaksMatrix = numpy.vstack([peaksMatrix, row])


# Create dataframe
df = pandas.DataFrame(peaksMatrix[1:], columns=peaksMatrix[0])
#print(df[:10])
features = df.columns[2:]
#print(features)
ionClasses = numpy.array(["noise","c","a","b","z","x","y"])
#print(type(ionClasses))
#print(ionClasses)
ionClassFact = pandas.factorize(df['ionClass'])[0]
df['ionClassFact'] = ionClassFact
#print(ionClassFact)


####################
# Cross Validation #
####################

print(df.shape, ionClassFact.shape)
 
X_train, X_test, y_train, y_test = train_test_split(df[features], ionClassFact, test_size=0.4, random_state=0)
 
print(X_train.shape, y_train.shape)
 
print(X_test.shape, y_test.shape)
 
 
clf = svm.SVC(kernel='linear', C=1).fit(X_train, y_train)
print(clf.score(X_test, y_test))

#from sklearn.model_selection import cross_val_score
#clf = svm.SVC(kernel='linear', C=1)

scores = cross_val_score(clf, df[features], ionClassFact, cv=5)
print(scores)                                              
print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))



#################
# Random Forest #
#################
# # Fitting
# clf = RandomForestClassifier(n_jobs=-1)
# df['is_train'] = numpy.random.uniform(0, 1, len(df)) <= .5
# train, test = df[df['is_train']==True], df[df['is_train']==False]
# #train = df[:100]
# fit = clf.fit(train[features], train['ionClassFact'])
# print(fit)
# 
# # Prediction
# print(clf.predict(test[features]))
# print(clf.predict_proba(df[features]))
# preds = ionClasses[clf.predict(test[features])]
# print(preds)
#  
#  
# # Create confusion matrix
# print(pandas.crosstab(test['ionClass'], preds, rownames=['Actual Ion Class'], colnames=['Predicted Ion Class']))
#  
# # View a list of the features and their importance scores
# print(list(zip(test[features], clf.feature_importances_)))


# tbcMatrix = [peaks[0].getHeader()]          
# with open(tbcFile) as inputFile:
#     for line in inputFile:
#         if(line[0].isdigit()):
#             lineSplit = line.split(" ")[:-1]
#             lineSplit = lineSplit[2:4]+lineSplit[0:2]+lineSplit[4:]
#             tbcMatrix = numpy.vstack([tbcMatrix, lineSplit])
# inputFile.close()
# tbcData = pandas.DataFrame(tbcMatrix[1:], columns=tbcMatrix[0])
# preds = ionClasses[clf.predict(tbcData[features])]
# 
# print(clf.predict(tbcData[features]))
# print(clf.predict_proba(tbcData[features]))
# print(preds)
# 
# print(tbcData['ionClass'].head())
# 
# # Create confusion matrix
# print(pandas.crosstab(tbcData['ionClass'], preds, rownames=['Actual Ion Class'], colnames=['Predicted Ion Class']))
# 
# # View a list of the features and their importance scores
# print(list(zip(tbcData[features], clf.feature_importances_)))


