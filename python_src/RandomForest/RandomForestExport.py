'''
Created on 05.07.2017

@author: HartkopfF
'''

import Input
import os
import sys
import pandas
import numpy
from sklearn.ensemble import RandomForestClassifier
from sklearn.externals.six import StringIO  
import pydot
from sklearn import tree
import six
from sklearn.tree import export_graphviz

if __name__ == '__main__':
    pass




# Change directory
os.chdir("../..")
cwd = os.getcwd()
print(cwd)


# Import peaks
file = sys.argv[1]
if os.path.exists(file):
    print(os.path.basename(file))
peaksTrain = Input.readTrainingsSet(file)
print(len(peaksTrain), "Training peaks were imported.")


tbcFile = sys.argv[2]
if os.path.exists(tbcFile):
    print(os.path.basename(tbcFile))

####################
# Data Preparation #
####################
fraction = 1.0
rootFileName = file.split("/")[-1][:-16]
outputFile = rootFileName + "_classified_"+str(fraction)+".mgf"

# Balance trainings dataset

peaksTrain = Input.balanceData(peaksTrain)

# Create matrix out of all peaks
peaksMatrix = [peaksTrain[0].getHeader()]
for peak in peaksTrain:
    if(numpy.random.uniform(0, 1,)<fraction):
        row = peak.getAsRow()
        peaksMatrix = numpy.vstack([peaksMatrix, row])
        steps = (len(peaksMatrix)/len(peaksTrain))*100
        print("%.2f" % steps, "%")



# Create dataframe
df = pandas.DataFrame(peaksMatrix[1:], columns=peaksMatrix[0])
features = df.columns[2:]
ionClassFact, ionClassDict = pandas.factorize(df['ionClass'])
ionClassDict = dict(zip(range(7), ionClassDict))
print(ionClassDict)
df['ionClassFact'] = ionClassFact
print(df['ionClass'])
print(ionClassFact)



#################
# Random Forest #
#################
# Fitting
clf = RandomForestClassifier(n_jobs=-1, max_depth=5, n_estimators=128)
fit = clf.fit(df[features], df['ionClassFact'])
print(fit)



dot_data = six.StringIO()

i_tree = 0
for tree_in_forest in clf.estimators_:
    i_tree = i_tree + 1   
    treeFile = "tree.dot" 
    export_graphviz(tree_in_forest,
        feature_names = features,
        filled=True,
        rounded=True,
        out_file=treeFile)
        
#####################
# Predict test data #
#####################

if not os.path.exists("output"):
        os.makedirs("output")
if not os.path.exists("output/randomforest"):
        os.makedirs("output/randomforest")
     
 
output = open("output/randomforest/"+outputFile, 'w')


print("Start writing to output/randomforest/"+outputFile)  
tbcMatrix = [peaksTrain[0].getHeader()]          
process = 0
with open(tbcFile) as Input:
    for tbcline in Input:
        if(tbcline[:3]=="END" and len(tbcMatrix)>1):
            print("Starting classifier for spectrum")
            tbcData = pandas.DataFrame(tbcMatrix[1:], columns=tbcMatrix[0])
            
            # Create classification dictionary and apply it
            
            preds = clf.predict(tbcData[features])
            print(preds)
            preds = numpy.vectorize(ionClassDict.get)(preds)
            print(preds)
            print(clf.predict_proba(tbcData[features]))

            # Rearrange columns
            colnames = df.columns.tolist()
            colnames = colnames[2:4]+colnames[0:2]+colnames[4:-1]
            tbcData = tbcData[colnames]
            
            outputLine = tbcData.to_string(header=False, index=False, index_names=False).split('\n')
            for index in range(len(tbcData)):
                row = outputLine[index]+"\n"
                row = ' '.join(row.split())+"\n"
                row = row.replace('?', preds[index], 1)
                output.write(row)
            tbcMatrix = [peaksTrain[0].getHeader()] 
            output.write(tbcline)       
       
        elif(tbcline[0].isdigit()):
            lineSplit = tbcline.split(" ")[:-1]
            lineSplit = lineSplit[2:4]+lineSplit[0:2]+lineSplit[4:]
            tbcMatrix = numpy.vstack([tbcMatrix, lineSplit])
            print(process, "peaks processed.")
            process = process+1
            
        elif(len(tbcMatrix)<1):
            print("Empty spectrum deleted")    
        else:
            output.write(tbcline)
 
output.close()        

