'''
Created on 19.08.2017

@author: Bready
'''
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

colors = ["b","g","r","c","m","y","k","w"]
# for name, hex in matplotlib.colors.cnames.items():
#     if("red" in name):
#         colors.append(name)
#     if("blue" in name):
#         colors.append(name)
#     if("green" in name):
#         colors.append(name)
#     if("yellow" in name):
#         colors.append(name)
#     print(name)
color0 = colors[0]
color1 = colors[1]
color2 = colors[2]
color3 = colors[3]
color4 = colors[4]
color5 = colors[5]

font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 22}

plt.rc('font', **font)

##############################
# processed/unprocessed plot #
##############################
N = 6

ind = np.arange(N)  # the x locations for the groups
width = 0.10       # the width of the bars

fig, ax = plt.subplots(figsize=(20, 10))
recall = (0.9787339,0.876095549,0.896875391,0.951871931,0.953856562,0.954505632)
rects1 = ax.bar(ind, recall, width, color=color0)

specificity = (1,0.998583724,0.973249414,0.991710041,0.997369629,0.9989475318)
rects2 = ax.bar(ind + width, specificity, width, color=color1)

precision = (1,0.994319429,0.910413433,0.92408143,0.976139934,0.990246853)
rects3 = ax.bar(ind + 2*width, precision, width, color=color2)

npv = (0.997371069,0.966080764,0.968882394,0.994881753,0.994807611,0.994927337)
rects4 = ax.bar(ind + 3*width, npv, width, color=color3)

f1 = (0.989252673,0.931471183,0.903593707,0.937770835,0.964869609,0.972047811)
rects5 = ax.bar(ind + 4*width, f1, width, color=color4)

accuracy = (0.997654817,0.971568404,0.955484533,0.987891682,0.992958292,0.994473114)
rects6 = ax.bar(ind + 5*width, accuracy, width, color=color5)



# add some text for labels, title and axes ticks
ax.set_ylabel('Recall')
ax.set_title('Validation of Random Forest with Training Sets')
ax.set_xticks(ind + width / 5+0.28)
ax.set_xticklabels(('synthetic\nCID', 'synthetic\nHCD', 'pyrococcus\nfuriosus\nHCD', 'human\nCID', 'yeast\nCID', 'UPS1 yeast\nCID'))

ax.legend((rects1[0], rects2[0],rects3[0],rects4[0],rects5[0],rects6[0]), ("recall","specificity","precision","negative\npredictive\nvalue","F1-score","accuracy"),bbox_to_anchor=(1.12, 0.5), loc=5, prop={'size': 20})
fig.savefig('validation.png')

plt.show()