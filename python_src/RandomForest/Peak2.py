'''
Created on 30.06.2017

@author: hartkopff
'''

class Peak(object):
    '''
    Class to hold spectra data
    '''
    mz = 0.0
    intensity = 0.0 
    ionClass = ""
    charge = 0
        
    def __init__(self):
        '''
        Constructor
        '''
    
    def getAsRow(self):
        row = [ self.ionClass, self.charge, self.mz, self.intensity]
        return(row)
    
    def getHeader(self):
        header = ["ionClass", "charge", "mz", "intensity"]
        
        return(header)
        
        
        
        