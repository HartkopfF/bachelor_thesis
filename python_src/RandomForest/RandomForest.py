'''
Created on 05.07.2017

@author: HartkopfF
'''

import Input
import os
import sys
import pandas
import numpy
from sklearn.ensemble import RandomForestClassifier


if __name__ == '__main__':
    pass




# Change directory
os.chdir("../..")
cwd = os.getcwd()
print(cwd)

# Logging
log = open("output/RandomForest.log","w") 
log.write(cwd+ '\n') 



# Import peaks
file = sys.argv[1]
if os.path.exists(file):
    print(os.path.basename(file))
    log.write(os.path.basename(file)+ '\n') 
tbcFile = sys.argv[1]
if os.path.exists(tbcFile):
    print(os.path.basename(tbcFile))
    log.write(os.path.basename(tbcFile)+ '\n') 
file = "res/spectra/UPS1/UPS1_12500amol_R1.mgf_traning.mgf"
tbcFile = "res/spectra/UPS1/UPS1_12500amol_R1.mgf_tbc.mgf"
peaks = Input.readTrainingsSet(file)
print(len(peaks), "peaks were imported.")
log.write(str(len(peaks))+ "peaks were imported."+ '\n') 


####################
# Data Preparation #
####################
# Create matrix out of all peaks
peaksMatrix = [peaks[0].getHeader()]
for peak in peaks:
    row = peak.getAsRow()
    peaksMatrix = numpy.vstack([peaksMatrix, row])


# Create dataframe
df = pandas.DataFrame(peaksMatrix[1:], columns=peaksMatrix[0])
#print(df[:10])
features = df.columns[2:]
#print(features)
ionClasses = numpy.array(["noise","c","a","b","z","x","y"])
#print(type(ionClasses))
#print(ionClasses)
ionClassFact = pandas.factorize(df['ionClass'])[0]
df['ionClassFact'] = ionClassFact
#print(ionClassFact)


#################
# Random Forest #
#################
# Fitting
clf = RandomForestClassifier(n_jobs=-1)
train = df
fit = clf.fit(train[features], train['ionClassFact'])
print(fit)
log.write(str(fit)+ '\n') 




tbcMatrix = [peaks[0].getHeader()]          
with open(tbcFile) as inputFile:
    for line in inputFile:
        if(line[0].isdigit()):
            lineSplit = line.split(" ")[:-1]
            lineSplit = lineSplit[2:4]+lineSplit[0:2]+lineSplit[4:]
            tbcMatrix = numpy.vstack([tbcMatrix, lineSplit])
inputFile.close()
tbcData = pandas.DataFrame(tbcMatrix[1:], columns=tbcMatrix[0])
preds = ionClasses[clf.predict(tbcData[features])]
 
print(clf.predict(tbcData[features]))
print(clf.predict_proba(tbcData[features]))
print(preds)
log.write(str(clf.predict(tbcData[features]))+ '\n') 
log.write(str(clf.predict_proba(tbcData[features]))+ '\n') 
log.write(str(preds)+ '\n') 

 
print(tbcData['ionClass'].head())
log.write(str(tbcData['ionClass'].head())+ '\n') 

# Create confusion matrix
print(pandas.crosstab(tbcData['ionClass'], preds, rownames=['Actual Ion Class'], colnames=['Predicted Ion Class']))
log.write(str(pandas.crosstab(tbcData['ionClass'], preds, rownames=['Actual Ion Class'], colnames=['Predicted Ion Class'])+ '\n')) 
 
# View a list of the features and their importance scores
print(list(zip(tbcData[features], clf.feature_importances_)))
log.write(str(list(zip(tbcData[features], clf.feature_importances_))+ '\n'))

log.close() 
