'''
Created on 04.08.2017

@author: hartkopff
'''
import re

def addScanNumber(file):  # @NoSelf

        
        # Read file into an array
        with open(file) as f:
            content = f.readlines()
            content = [x.strip() for x in content] 
            
        output = file+"_new"
        f = open(output, 'w')
        scan = 1

        for line in content:
            #print(line)
            if(len(line)):
                firstElement = line[0]
            else:
                firstElement = ""
                
                
            if(firstElement=='C'):
                f.write(line+"\n")
                scanLine = "SCANS="+str(scan)+"\n"   
                f.write(scanLine)
                scan+=1
            elif(firstElement == "S"):
                print("skipped")
            elif(firstElement == "T"):
                m = re.match(r".*?=(\d*)\".*",line)
                scan = int(m.group(1))
                print(m.group(1))
                f.write(line+"\n")#+" scan="+str(scan)+"\"\n")
            else:
                f.write(line+"\n")


        f.close() 

print("starting...")
addScanNumber("C:/Users/hartkopff/Workspace/bachelor_thesis/res/spectra/UPS1/UPS1_12500amol_R1.mgf")
print("done")