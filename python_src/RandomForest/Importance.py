'''
Created on 19.08.2017

@author: Bready
'''
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
import matplotlib
from colour import Color 
import colorsys


fontP = FontProperties()
fontP.set_size(18)

colors = []
# for name, hex in matplotlib.colors.cnames.items():
#     if("red" in name):
#         colors.append(name)
#     if("blue" in name):
#         colors.append(name)
#     if("green" in name):
#         colors.append(name)
#     print(name)
# print(len(colors))
# print(colors)

N = 23
HSV_tuples = [(x*1.0/N, 0.5, 0.5) for x in range(N)]
colors = map(lambda x: colorsys.hsv_to_rgb(*x), HSV_tuples)
i = 0
print(colors)
red = Color("red")
colors = list(red.range_to(Color("purple"),24))
color0 = str(colors[0])
color1 = str(colors[1])
color2 = str(colors[2])
color3 = str(colors[3])
color4 = str(colors[4])
color5 = str(colors[5])
color6 = str(colors[6])
color7 = str(colors[7])
color8 = str(colors[8])
color9 = str(colors[9])
color10 = str(colors[10])
color11 = str(colors[11])
color12 = str(colors[12])
color13 = str(colors[13])
color14 = str(colors[14])
color15 = str(colors[15])
color16 = str(colors[16])
color17 = str(colors[17])
color18 = str(colors[18])
color19 = str(colors[19])
color20 = str(colors[20])
color21 = str(colors[21])
color22 = str(colors[22])
print(color2)

font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 22}

plt.rc('font', **font)

##############################
# processed/unprocessed plot #
##############################
N = 6

ind = np.arange(N)  # the x locations for the groups
width = 0.028      # the width of the bars

fig, ax = plt.subplots(figsize=(20, 10))


recall = (0.02549341,0.018103981,0.021152565,0.027829544,0.032950711,0.038876079)
rects1 = ax.bar(ind, recall, width, color=color0)

specificity = (0.077610556,0.037956526,0.028286074,0.071097658,0.07084183,0.069854678)
rects2 = ax.bar(ind + width, specificity, width, color=color1)

precision = (0.098272247,0.12052513,0.133264945,0.113324582,0.119948145,0.11309469)
rects3 = ax.bar(ind + 2*width, precision, width, color=color2)

npv = (0.059553732,0.061502638,0.06729723,0.050256604,0.057545169,0.062311428)
rects4 = ax.bar(ind + 3*width, npv, width, color=color3)

f1 = (0.000248249,7.39E-05,0,0.001745847,0.000954148,0.000448993)
rects5 = ax.bar(ind + 4*width, f1, width, color=color4)

accuracy = (0.145153748,0.092851905,0.1049007,0.102300147,0.11035187,0.106214477)
rects6 = ax.bar(ind + 5*width, accuracy, width, color=color5)

recall = (0.117823005,0.058211116,0.045188396,0.089476649,0.087706439,0.090033102)
rects7 = ax.bar(ind+ 6*width, recall, width, color=color6)

specificity = (0.036139713,0.100666402,0.088173287,0.040924847,0.04355755,0.047195778)
rects8 = ax.bar(ind + 7*width, specificity, width, color=color7)

precision = (0.011937081,0.05224483,0.07083876,0.019840412,0.019766923,0.019960553)
rects9 = ax.bar(ind + 8*width, precision, width, color=color8)

npv = (0.010741064,0.004081035,0.00808532,0.013748178,0.002753381,0.003821571)
rects10 = ax.bar(ind + 9*width, npv, width, color=color9)

f1 = (0.014319472,0.008430614,0.004501912,0.019369059,0.009774572,0.00926148)
rects11 = ax.bar(ind + 10*width, f1, width, color=color10)

accuracy = (0.022816837,0.0311407,0.025227281,0.022302748,0.020517121,0.017439057)
rects12 = ax.bar(ind + 11*width, accuracy, width, color=color11)

recall = (0.038045889,0.040077457,0.052718127,0.039214751,0.046653159,0.045515647)
rects13 = ax.bar(ind+ 12*width, recall, width, color=color12)

specificity = (0.018046049,0.013608181,0.00943064,0.027016667,0.019484393,0.020080117)
rects14 = ax.bar(ind + 13*width, specificity, width, color=color13)

precision = (0.053000315,0.03671991,0.029860799,0.062144851,0.065698989,0.062665498)
rects15 = ax.bar(ind + 14*width, precision, width, color=color14)

npv = (0.011366284,0.046077407,0.035327447,0.028771768,0.015289518,0.01684769)
rects16 = ax.bar(ind + 15*width, npv, width, color=color15)

f1 = (0.126055968,0.169818222,0.149141836,0.115788116,0.113731831,0.121392156)
rects17 = ax.bar(ind + 16*width, f1, width, color=color16)

accuracy = (0.093378184,0.050069751,0.055349085,0.076885938,0.08952636,0.084516778)
rects18 = ax.bar(ind + 17*width, accuracy, width, color=color17)

recall = (0.000931026,0.00060846,0.001430808,0.001320635,0.003191742,0.003497875)
rects19 = ax.bar(ind+ 18*width, recall, width, color=color18)

specificity = (0.002209606,0.002801676,0.001430622,0.005565815,0.004009567,0.003608126)
rects20 = ax.bar(ind + 19*width, specificity, width, color=color19)

precision = (0.010302232,0.035243697,0.035677169,0.033998745,0.023492995,0.021413618)
rects21 = ax.bar(ind + 20*width, precision, width, color=color20)

npv = (0.025798742,0.018025586,0.029612314,0.034356776,0.040146857,0.040570441)
rects22 = ax.bar(ind + 21*width, npv, width, color=color21)

f1 = (0.000756591,0.001160869,0.003104682,0.002719663,0.002106731,0.001380166)
rects23 = ax.bar(ind + 22*width, f1, width, color=color22)


# add some text for labels, title and axes ticks
ax.set_ylabel('GINI importance (%)')
ax.set_title('GINI Importance of Random Forest')
ax.set_xticks(ind + width / 5+0.345)
ax.set_xticklabels(('synthetic\nCID', 'synthetic\nHCD', 'pyrococcus\nfuriosus\nHCD', 'human\nCID', 'yeast\nCID', 'UPS1 yeast\nCID'))
#plt.tight_layout()
ax.legend((rects1[0], rects2[0],rects3[0],rects4[0],rects5[0],rects6[0],rects7[0], rects8[0],rects9[0],rects10[0],rects11[0],rects12[0],rects13[0], rects14[0],rects15[0],rects16[0],rects17[0],rects18[0],rects19[0], rects20[0],rects21[0],rects22[0],rects23[0]), ('ch4Gain', 'ch4Loss', 'coGain', 'coLoss', 'complIon', 'h2oGain', 'h2oLoss', 'hGain', 'hLoss', 'intBasePeak', 'intensity', 'intLocal\nBasePeak', 'intLocalTotal\nIonCurrent', 'intTotal\nIonCurrent', 'localRank', 'mz', 'nh3Gain', 'nh3Loss', 'pCharge', 'pepmass', 'pepmassMz', 'rank', 'rtenSec'),ncol=1, prop = fontP, loc='right', bbox_to_anchor=(1.13, 0.5))
fig.savefig('features.png')

plt.show()