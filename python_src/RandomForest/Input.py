'''
Created on 30.06.2017

@author: hartkopff
'''
import Peak  # @UnresolvedImport
import random
import pandas
import numpy
import progressbar as pb
from random import shuffle

def readTrainingsSet(file):  # @NoSelf

        
        # Read file into an array
        with open(file) as f:
            content = f.readlines()
            content = [x.strip() for x in content] 
            
        peaks = []

        for line in content:
            firstElement = line[0]
            
            if(firstElement.isdigit()):
                lineSplit = line.split(" ")
                tmpPeak = Peak.Peak()
                tmpPeak.mz = lineSplit[0]
                tmpPeak.intensity = lineSplit[1]
                tmpPeak.ionClass = lineSplit[2]
                tmpPeak.charge = lineSplit[3]
                tmpPeak.rank = lineSplit[4]
                tmpPeak.intBasePeak = lineSplit[5]
                tmpPeak.intTotalIonCurrent = lineSplit[6]
                tmpPeak.localRank = lineSplit[7]
                tmpPeak.intLocalBasePeak = lineSplit[8]
                tmpPeak.intLocalTotalIonCurrent = lineSplit[9]
                tmpPeak.h2oLoss = lineSplit[10]
                tmpPeak.ch4Loss = lineSplit[11]
                tmpPeak.nh3Loss = lineSplit[12]
                tmpPeak.coLoss = lineSplit[13]
                tmpPeak.hLoss = lineSplit[14]
                tmpPeak.h2oGain = lineSplit[15]
                tmpPeak.ch4Gain = lineSplit[16]
                tmpPeak.nh3Gain = lineSplit[17]
                tmpPeak.coGain = lineSplit[18]
                tmpPeak.hGain = lineSplit[19]
                tmpPeak.pCharge = lineSplit[20]
                tmpPeak.rtenSec = lineSplit[21]
                tmpPeak.pepmass = lineSplit[22]
                tmpPeak.pepmassMz = lineSplit[23]
                tmpPeak.complIon = lineSplit[24]
   
                
                peaks.append(tmpPeak)

                
                        
        return peaks
 
 
    
def readTrainingsSetDataframe(file):  # @NoSelf
        

        
        # Read file into an array
        with open(file) as f:
            content = f.readlines()
            content = [x.strip() for x in content] 
        
        # Progressbar
        widgets = ['Reading peaks from trainings set: ', pb.Percentage(), ' ', pb.Bar(marker=pb.RotatingMarker()), ' ', pb.ETA()]
        timer = pb.ProgressBar(widgets=widgets, maxval=len(content)).start()
            
        header = ["ionClass", "charge", "mz", "intensity", "rank", "intBasePeak", "intTotalIonCurrent", "localRank", "intLocalBasePeak", "intLocalTotalIonCurrent", "h2oLoss", "ch4Loss", "nh3Loss", "coLoss", "hLoss", "h2oGain", "ch4Gain", "nh3Gain", "coGain", "hGain", "pCharge", "rtenSec", "pepmass", "pepmassMz", "complIon"]
        index = 1
        counter = 0
        #peaksMatrix = pandas.DataFrame([], columns=header)
        peaksArray = []

        for line in content:
            firstElement = line[0]
            
            if(firstElement.isdigit()):
                lineSplit = line.split(" ") 
                lineSplit = lineSplit[2:4]+lineSplit[:2]+lineSplit[4:]
                peaksArray.append( lineSplit)
                #peaksMatrix.loc[index] = lineSplit
                index+=1
            counter+=1
            timer.update(counter) 
        
        peaksMatrix = pandas.DataFrame(data=peaksArray,    # values
                     index=numpy.arange(0,index-1,1),    # 1st column as index
                     columns=header)  # 1st row as the column names
        peaksMatrix['charge'] = peaksMatrix['charge'].replace('?', 1)
        return peaksMatrix    
    
    
    
    
    
def balanceData(peaks):
    print("Start balancing data...")
    print(len(peaks)," peaks before balancing data...")

    aList = []
    bList = []
    cList = []
    xList = []
    yList = [] 
    zList = [] 
    noiseList = []   
    
     
    # Count peaks
    for peak in peaks:
        if  peak.ionClass == "a":
            aList.append(peak)
        elif  peak.ionClass == "b": 
            bList.append(peak)
        elif  peak.ionClass == "c":
            cList.append(peak)
        elif  peak.ionClass == "x": 
            xList.append(peak)
        elif  peak.ionClass == "y":
            yList.append(peak)
        elif  peak.ionClass == "z": 
            zList.append(peak)
        elif  peak.ionClass == "noise":
            noiseList.append(peak)
    
    # Set to lowest amount
    ionList = [len(aList),len(bList),len(cList),len(xList),len(yList),len(zList),len(noiseList)]
    print(["a","b","c","x","y","z","noise"])
    print(ionList)
    minimum = min(ionList)
    print("Minimum amount of ions: ",minimum)
    
    
    # Shuffle peaks
     
    random.shuffle(aList)
    random.shuffle(bList)
    random.shuffle(cList)
    random.shuffle(xList)
    random.shuffle(yList)
    random.shuffle(zList)
    random.shuffle(noiseList)
    
    # Remove randomly selected peaks
    peaks = []
    
    for i in range(1,minimum):
        print("Added peaks: "+str(i*7))
        peaks.append(aList.pop())
        peaks.append(bList.pop())
        peaks.append(cList.pop())
        peaks.append(xList.pop())
        peaks.append(yList.pop())
        peaks.append(zList.pop())
        peaks.append(noiseList.pop())


    
    
    print(len(peaks)," peaks after balancing data...")

    return peaks
    
def balanceValidation(train):
    print("Start balancing data...")
    print(len(train)," peaks before balancing data...")
    
    # Progressbar
    widgets = ['Reading peaks from trainings set: ', pb.Percentage(), ' ', pb.Bar(marker=pb.RotatingMarker()), ' ', pb.ETA()]
    timer = pb.ProgressBar(widgets=widgets, maxval=len(train)).start()
    counter = 0
    
    
    aList = pandas.DataFrame()
    bList = pandas.DataFrame()
    cList = pandas.DataFrame()
    xList = pandas.DataFrame()
    yList = pandas.DataFrame()
    zList = pandas.DataFrame()
    noiseList = pandas.DataFrame()
    
     
    # Count peaks
    for row in train.iterrows():
        if row["ionClass"]== "a":
            aList = aList.append(row)
        elif row["ionClass"] == "b": 
            bList = bList.append(row)
        elif row["ionClass"] == "c":
            cList = cList.append(row)
        elif row["ionClass"] == "x": 
            xList = xList.append(row)
        elif row["ionClass"] == "y":
            yList = yList.append(row)
        elif row["ionClass"] == "z": 
            zList = zList.append(row)
        elif row["ionClass"] == "noise":
            noiseList = noiseList.append(row)
        counter+=1
        timer.update(counter)    

        
    
    # Set to lowest amount
    ionList = [aList.shape[0],bList.shape[0],cList.shape[0],xList.shape[0],yList.shape[0],zList.shape[0],noiseList.shape[0]]
    print(["a","b","c","x","y","z","noise"])
    print(ionList)
    minimum = min(ionList)
    print("Minimum amount of ions: ",minimum)
     
     
    # Shuffle peaks     
    aList = aList.sample(frac=1)
    bList = bList.sample(frac=1)
    cList = cList.sample(frac=1)
    xList = xList.sample(frac=1)
    yList = yList.sample(frac=1)
    zList = zList.sample(frac=1)
    noiseList = noiseList.sample(frac=1)

     
    # Remove randomly selected peaks
    peaks = pandas.DataFrame()
    peaks = peaks.append(aList[:minimum]) 
    peaks = peaks.append(bList[:minimum]) 
    peaks = peaks.append(cList[:minimum]) 
    peaks = peaks.append(xList[:minimum]) 
    peaks = peaks.append(yList[:minimum]) 
    peaks = peaks.append(zList[:minimum])
    peaks = peaks.append(noiseList[:minimum]) 
    
    print(peaks.columns)
      
    print(peaks.shape[0]," peaks after balancing data...")

    return peaks

def balanceValidationFast(train):
    print("Start balancing data...")
    print(len(train)," peaks before balancing data...")
    
    # Progressbar
    widgets = ['Reading peaks from trainings set: ', pb.Percentage(), ' ', pb.Bar(marker=pb.RotatingMarker()), ' ', pb.ETA()]
    timer = pb.ProgressBar(widgets=widgets, maxval=len(train)).start()
    counter = 0
    
    
    aList = []
    bList = []
    cList = []
    xList = []
    yList = []
    zList = []
    noiseList = []
    
    trainArray = train.as_matrix()
    # Count peaks
    for i in range(0,len(trainArray)):
        row = trainArray[i]
        if row[0]== "a":
            aList.append([row])
        elif row[0] == "b": 
            bList.append(row)
        elif row[0] == "c":
            cList.append(row)
        elif row[0] == "x": 
            xList.append(row)
        elif row[0] == "y":
            yList.append(row)
        elif row[0] == "z": 
            zList.append(row)
        elif row[0] == "noise":
            noiseList.append(row)
        counter+=1
        timer.update(counter)    

        
    print(len(noiseList))
    print(len(noiseList[0]))
    print(type(aList))


    # Set to lowest amount
    ionList = [len(aList),len(bList),len(cList),len(xList),len(yList),len(zList),len(noiseList)]
    print(["a","b","c","x","y","z","noise"])
    print(ionList)
    minimum = min(ionList)
    print("Minimum amount of ions: ",minimum)
    print(type(aList))

     
    # Shuffle peaks     
    shuffle(aList)
    shuffle(bList)
    shuffle(cList)
    shuffle(xList)
    shuffle(yList)
    shuffle(zList)
    shuffle(noiseList)
    print(noiseList)
    print(len(noiseList[0]))
    print(type(aList))

    # Remove randomly selected peaks
    peaksArray = []
    for i in range(0,minimum):
            peaksArray.append(aList[i])
            peaksArray.append(bList[i])
            peaksArray.append(cList[i])
            peaksArray.append(xList[i])
            peaksArray.append(yList[i])
            peaksArray.append(zList[i])
            peaksArray.append(noiseList[i])

    
    print(type(peaksArray))
    print(len(peaksArray))
    print(len(peaksArray[0]))

    header = ["ionClass", "charge", "mz", "intensity", "rank", "intBasePeak", "intTotalIonCurrent", "localRank", "intLocalBasePeak", "intLocalTotalIonCurrent", "h2oLoss", "ch4Loss", "nh3Loss", "coLoss", "hLoss", "h2oGain", "ch4Gain", "nh3Gain", "coGain", "hGain", "pCharge", "rtenSec", "pepmass", "pepmassMz", "complIon"]

    peaks = pandas.DataFrame(data=peaksArray,    # values
                     index=numpy.arange(0,counter,1),    # 1st column as index
                     columns=header)  # 1st row as the column names    
    print(peaks.columns)
      
    print(peaks.shape[0]," peaks after balancing data...")

    return peaks
    
