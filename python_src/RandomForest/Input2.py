'''
Created on 30.06.2017

@author: hartkopff
'''
import Peak2 as Peak  # @UnresolvedImport
import random
import pandas

def readTrainingsSet(file):  # @NoSelf

        
        # Read file into an array
        with open(file) as f:
            content = f.readlines()
            content = [x.strip() for x in content] 
            
        peaks = []

        for line in content:
            firstElement = line[0]
            
            if(firstElement.isdigit()):
                lineSplit = line.split(" ")
                tmpPeak = Peak.Peak()
                tmpPeak.mz = lineSplit[0]
                tmpPeak.intensity = lineSplit[1]
                tmpPeak.ionClass = lineSplit[2]
                tmpPeak.charge = lineSplit[3]
   
                
                peaks.append(tmpPeak)

                
                        
        return peaks
    
def balanceData(peaks):
    print("Start balancing data...")
    print(len(peaks)," peaks before balancing data...")

    aList = []
    bList = []
    cList = []
    xList = []
    yList = [] 
    zList = [] 
    noiseList = []   
    
     
    # Count peaks
    for peak in peaks:
        if  peak.ionClass == "a":
            aList.append(peak)
        elif  peak.ionClass == "b": 
            bList.append(peak)
        elif  peak.ionClass == "c":
            cList.append(peak)
        elif  peak.ionClass == "x": 
            xList.append(peak)
        elif  peak.ionClass == "y":
            yList.append(peak)
        elif  peak.ionClass == "z": 
            zList.append(peak)
        elif  peak.ionClass == "noise":
            noiseList.append(peak)
    
    # Set to lowest amount
    ionList = [len(aList),len(bList),len(cList),len(xList),len(yList),len(zList),len(noiseList)]
    print(["a","b","c","x","y","z","noise"])
    print(ionList)
    minimum = min(ionList)
    print("Minimum amount of ions: ",minimum)
    
    
    # Shuffle peaks
     
    random.shuffle(aList)
    random.shuffle(bList)
    random.shuffle(cList)
    random.shuffle(xList)
    random.shuffle(yList)
    random.shuffle(zList)
    random.shuffle(noiseList)
    
    # Remove randomly selected peaks
    peaks = []
    
    for i in range(1,minimum):
        print("Added peaks: "+str(i*7))
        peaks.append(aList.pop())
        peaks.append(bList.pop())
        peaks.append(cList.pop())
        peaks.append(xList.pop())
        peaks.append(yList.pop())
        peaks.append(zList.pop())
        peaks.append(noiseList.pop())


    
    
    print(len(peaks)," peaks after balancing data...")

    return peaks
    
def balanceValidation(train):
    print("Start balancing data...")
    print(len(train)," peaks before balancing data...")

    aList = pandas.DataFrame()
    bList = pandas.DataFrame()
    cList = pandas.DataFrame()
    xList = pandas.DataFrame()
    yList = pandas.DataFrame()
    zList = pandas.DataFrame()
    noiseList = pandas.DataFrame()
    
     
    # Count peaks
    for row in train.iterrows():
        if row["ionClass"]== "a":
            aList = aList.append(row)
        elif row["ionClass"] == "b": 
            bList = bList.append(row)
        elif row["ionClass"] == "c":
            cList = cList.append(row)
        elif row["ionClass"] == "x": 
            xList = xList.append(row)
        elif row["ionClass"] == "y":
            yList = yList.append(row)
        elif row["ionClass"] == "z": 
            zList = zList.append(row)
        elif row["ionClass"] == "noise":
            noiseList = noiseList.append(row)
    
    print(bList)
    # Set to lowest amount
    ionList = [aList.shape[0],bList.shape[0],cList.shape[0],xList.shape[0],yList.shape[0],zList.shape[0],noiseList.shape[0]]
    print(["a","b","c","x","y","z","noise"])
    print(ionList)
    minimum = min(ionList)
    print("Minimum amount of ions: ",minimum)
     
     
    # Shuffle peaks     
    aList = aList.sample(frac=1)
    bList = bList.sample(frac=1)
    cList = cList.sample(frac=1)
    xList = xList.sample(frac=1)
    yList = yList.sample(frac=1)
    zList = zList.sample(frac=1)
    noiseList = noiseList.sample(frac=1)

     
    # Remove randomly selected peaks
    peaks = pandas.DataFrame()
    peaks = peaks.append(aList[:minimum]) 
    peaks = peaks.append(bList[:minimum]) 
    peaks = peaks.append(cList[:minimum]) 
    peaks = peaks.append(xList[:minimum]) 
    peaks = peaks.append(yList[:minimum]) 
    peaks = peaks.append(zList[:minimum])
    peaks = peaks.append(noiseList[:minimum]) 
    
    print(peaks.columns)
      
    print(peaks.shape[0]," peaks after balancing data...")

    return peaks
    
