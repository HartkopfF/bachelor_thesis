'''
Created on 01.08.2017

@author: Bready
'''
import pandas 
import numpy 
import matplotlib.pyplot as plt
import Input  # @UnresolvedImport
import glob
import os

if __name__ == '__main__':
    pass

# TOBEREMOVED (TBR)
#plt.ion()
#END TBR 

# Change directory
os.chdir("../..")
cwd = os.getcwd()
print(cwd)
list_of_files=glob.glob("res/spectra/*/*training.mgf")
titleNames = ['Training Set of UPS1 Data Set with Human Database', 'Training Set of  Pyrococcus Furiosus Data Set', 'Training Set of Synthetic CID Data Set', 'Training Set of synthetic HCD Data Set', 'Training Set of UPS1 Data Set with Yeast and UPS1 Database', 'Training Set of UPS1 Data Set with Yeast Database']

print(list_of_files)

for i_file in range(len(list_of_files)):
        
    # Import peaks
    ###file = "res/spectra/pyro/Pyro_Velos.mgf_training.mgf"
    file=list_of_files[i_file]
    print(file)
    peaks = Input.readTrainingsSetDataframe(file)
    print("\n",len(peaks), "peaks were imported.")
    
    # Create dataframe
    df = peaks
    features = df.columns[1:]
    ionClasses = ["a", "b", "c", "x", "y", "z", "noise"]
    
    meanFrame = numpy.zeros(shape=(7,24))
    index = 0
    for ion in ionClasses:
        ionFrame = df.loc[df["ionClass"]==ion]
        dfMean = pandas.DataFrame(ionFrame[features].astype(float).mean()).transpose()
        meanFrame[index] = dfMean.as_matrix()
        index+=1
        
    dfMean = pandas.DataFrame(meanFrame,columns=features)
    dfMean["ionClass"] = ionClasses
    print(dfMean)
    
    
    #barplot figure
    plt.figure()
    dfMean.boxplot()
    #plt.show()
    
    ax = dfMean[features].plot(kind='bar', title ="V comp", figsize=(70, 70), legend=True, fontsize=12)
    ax.set_ylabel("intensity", fontsize=12)
    ax.set_xlabel("ion class", fontsize=12)
    plt.xticks(numpy.arange(7), ionClasses)
    #plt.show()
    plt.savefig('barplot_old_test%s.png'%(i_file),  bbox_inches='tight' )
    
    
    
    
    
    
    
    #### here starts only working with meanFrame
    meanFrame_bar = numpy.zeros(shape=(24,7))
    index = 0
    for ion in ionClasses:
        ionFrame_bar = df.loc[df["ionClass"]==ion]
        dfMean_bar = pandas.DataFrame(ionFrame_bar[features].astype(float).mean())   #.transpose()
        meanFrame_bar[:,index] = dfMean_bar.as_matrix()[:,0]
        index+=1
    meanFrame_sc_bar=numpy.empty(meanFrame_bar.shape)
    for i in range(meanFrame_bar.shape[0]):
        meanFrame_sc_bar[i,:]= meanFrame_bar[i,:]/numpy.max(meanFrame_bar[i,:])
    meanFrame_nrm_bar=numpy.empty(meanFrame_bar.shape)
    for i in range(meanFrame_bar.shape[0]):
        meanFrame_nrm_bar[i,:]= meanFrame_bar[i,:]/numpy.sum(meanFrame_bar[i,:])       
        
    ###dfMean_bar = pandas.DataFrame(meanFrame_bar,columns=ionClasses)
    dfMean_bar = pandas.DataFrame(meanFrame_nrm_bar,columns=ionClasses)
    ###dfMean_bar = pandas.DataFrame(meanFrame_sc_bar,columns=ionClasses)
    dfMean_bar["features"] = features
    print(dfMean)
    
    ax = dfMean_bar[ionClasses].plot(kind='bar', title ="V comp", figsize=(70, 70), legend=True, fontsize=12)
    ax.set_ylabel("intensity", fontsize=12)
    ax.set_xlabel("feature", fontsize=12)
    plt.xticks(numpy.arange(24), features)
    #plt.show()
    plt.savefig('barplot_test_normed%s.png'%(i_file),  bbox_inches='tight' )
    
    # radar plot
    """
    https://matplotlib.org/examples/api/radar_chart.html
    ======================================
    Radar chart (aka spider or star chart)
    ======================================
    
    This example creates a radar chart, also known as a spider or star chart [1]_.
    
    Although this example allows a frame of either 'circle' or 'polygon', polygon
    frames don't have proper gridlines (the lines are circles instead of polygons).
    It's possible to get a polygon grid by setting GRIDLINE_INTERPOLATION_STEPS in
    matplotlib.axis to the desired number of vertices, but the orientation of the
    polygon is not aligned with the radial axes.
    
    .. [1] http://en.wikipedia.org/wiki/Radar_chart
    """
    import numpy as np
    
    from matplotlib.path import Path
    from matplotlib.spines import Spine
    from matplotlib.projections.polar import PolarAxes
    from matplotlib.projections import register_projection
    
    
    def radar_factory(num_vars, frame='circle'):
        """Create a radar chart with `num_vars` axes.
    
        This function creates a RadarAxes projection and registers it.
    
        Parameters
        ----------
        num_vars : int
            Number of variables for radar chart.
        frame : {'circle' | 'polygon'}
            Shape of frame surrounding axes.
    
        """
        # calculate evenly-spaced axis angles
        theta = np.linspace(0, 2*np.pi, num_vars, endpoint=False)
        # rotate theta such that the first axis is at the top
        theta += np.pi/2
    
        def draw_poly_patch(self):
            verts = unit_poly_verts(theta)
            return plt.Polygon(verts, closed=True, edgecolor='k')
    
        def draw_circle_patch(self):
            # unit circle centered on (0.5, 0.5)
            return plt.Circle((0.5, 0.5), 0.5)
    
        patch_dict = {'polygon': draw_poly_patch, 'circle': draw_circle_patch}
        if frame not in patch_dict:
            raise ValueError('unknown value for `frame`: %s' % frame)
    
        class RadarAxes(PolarAxes):
    
            name = 'radar'
            # use 1 line segment to connect specified points
            RESOLUTION = 1
            # define draw_frame method
            draw_patch = patch_dict[frame]
    
            def fill(self, *args, **kwargs):
                """Override fill so that line is closed by default"""
                closed = kwargs.pop('closed', True)
                return super(RadarAxes, self).fill(closed=closed, *args, **kwargs)
    
            def plot(self, *args, **kwargs):
                """Override plot so that line is closed by default"""
                lines = super(RadarAxes, self).plot(*args, **kwargs)
                for line in lines:
                    self._close_line(line)
    
            def _close_line(self, line):
                x, y = line.get_data()
                # FIXME: markers at x[0], y[0] get doubled-up
                if x[0] != x[-1]:
                    x = np.concatenate((x, [x[0]]))
                    y = np.concatenate((y, [y[0]]))
                    line.set_data(x, y)
    
            def set_varlabels(self, labels):
                self.set_thetagrids(np.degrees(theta), labels)
    
            def _gen_axes_patch(self):
                return self.draw_patch()
    
            def _gen_axes_spines(self):
                if frame == 'circle':
                    return PolarAxes._gen_axes_spines(self)
                # The following is a hack to get the spines (i.e. the axes frame)
                # to draw correctly for a polygon frame.
    
                # spine_type must be 'left', 'right', 'top', 'bottom', or `circle`.
                spine_type = 'circle'
                verts = unit_poly_verts(theta)
                # close off polygon by repeating first vertex
                verts.append(verts[0])
                path = Path(verts)
    
                spine = Spine(self, spine_type, path)
                spine.set_transform(self.transAxes)
                return {'polar': spine}
    
        register_projection(RadarAxes)
        return theta
    
    def unit_poly_verts(theta):
        """Return vertices of polygon for subplot axes.
    
        This polygon is circumscribed by a unit circle centered at (0.5, 0.5)
        """
        x0, y0, r = [0.5] * 3
        verts = [(r*np.cos(t) + x0, r*np.sin(t) + y0) for t in theta]
        return verts
    
    def example_data():
        data = [
            np.array(features),#['f%i'%(i+1) for i in range(24)], #['f1', 'f2', 'f3', ..., 'f23'],
            ('Pyro_Velos.mgf_training.mgf', [
               meanFrame[0,:],   # a  need to be 24 entries per line!
               meanFrame[1,:],   # b
               meanFrame[2,:],   # c
               meanFrame[3,:],   # x
               meanFrame[4,:],   # y
               meanFrame[5,:],   # z
               meanFrame[6,:]    # noise
                ])  
               ]
        return data
    
    def example_data_scaled():
        meanFrameNorm=np.empty(meanFrame.shape)
        for i in range(meanFrame.shape[1]):
            meanFrameNorm[:,i]= meanFrame[:,i]/np.max(meanFrame[:,i])
        data = [
            ['charge', 'mz', 'intensity', 'rank', 'int\nBasePeak', 'intTotal\nIonCurrent', 'local\nRank', 'intLocal\nBasePeak', 'intLocal\nTotal\nIonCurrent', 'h2o\nLoss', 'ch4\nLoss', 'nh3\nLoss', 'co\nLoss', 'h\nLoss', 'h2o\nGain', 'ch4\nGain', 'nh3\nGain', 'co\nGain', 'h\nGain', 'p\nCharge', 'rten\nSec', 'pepmass', 'pepmass\nMz', 'compl\nIon'], #np.array(features),
            ('a-x-noise', [
                           meanFrameNorm[0,:],   # a  need to be 24 entries per line!
                           ###meanFrameNorm[1,:],   # b
                           ###meanFrameNorm[2,:],   # c
                           meanFrameNorm[3,:],   # x
                           ###meanFrameNorm[4,:],   # y
                           ###meanFrameNorm[5,:],   # z
                           meanFrameNorm[6,:]]), # noise
            ('b-y-noise', [
                           ###meanFrameNorm[0,:],   # a  need to be 24 entries per line!
                           meanFrameNorm[1,:],   # b
                           ###meanFrameNorm[2,:],   # c
                           ###meanFrameNorm[3,:],   # x
                           meanFrameNorm[4,:],   # y
                           ###meanFrameNorm[5,:],   # z
                           meanFrameNorm[6,:]]), # noise
            ('c-z-noise', [
                           ###meanFrameNorm[0,:],   # a  need to be 24 entries per line!
                           ###meanFrameNorm[1,:],   # b
                           meanFrameNorm[2,:],   # c
                           ###meanFrameNorm[3,:],   # x
                           ###meanFrameNorm[4,:],   # y
                           meanFrameNorm[5,:],   # z
                           meanFrameNorm[6,:]])  # noise          
                           ]
        return data
    
    
    def example_data_normalized():
        meanFrameNorm=np.empty(meanFrame.shape)
        for i in range(meanFrame.shape[1]):
            if np.sum(meanFrame[:,i]) >0:
                meanFrameNorm[:,i]= meanFrame[:,i]/np.sum(meanFrame[:,i])
            else:
                meanFrameNorm[:,i]= 0.
        data = [
            ['charge', 'mz', 'intensity', 'rank', 'int\nBasePeak', 'intTotal\nIonCurrent', 'local\nRank', 'intLocal\nBasePeak', 'intLocal\nTotal\nIonCurrent', 'h2o\nLoss', 'ch4\nLoss', 'nh3\nLoss', 'co\nLoss', 'h\nLoss', 'h2o\nGain', 'ch4\nGain', 'nh3\nGain', 'co\nGain', 'h\nGain', 'p\nCharge', 'rten\nSec', 'pepmass', 'pepmass\nMz', 'compl\nIon'], #np.array(features),
            ('a-x-noise', [
                           meanFrameNorm[0,:],   # a  need to be 24 entries per line!
                           ###meanFrameNorm[1,:],   # b
                           ###meanFrameNorm[2,:],   # c
                           meanFrameNorm[3,:],   # x
                           ###meanFrameNorm[4,:],   # y
                           ###meanFrameNorm[5,:],   # z
                           meanFrameNorm[6,:]]), # noise
            ('b-y-noise', [
                           ###meanFrameNorm[0,:],   # a  need to be 24 entries per line!
                           meanFrameNorm[1,:],   # b
                           ###meanFrameNorm[2,:],   # c
                           ###meanFrameNorm[3,:],   # x
                           meanFrameNorm[4,:],   # y
                           ###meanFrameNorm[5,:],   # z
                           meanFrameNorm[6,:]]), # noise
            ('c-z-noise', [
                           ###meanFrameNorm[0,:],   # a  need to be 24 entries per line!
                           ###meanFrameNorm[1,:],   # b
                           meanFrameNorm[2,:],   # c
                           ###meanFrameNorm[3,:],   # x
                           ###meanFrameNorm[4,:],   # y
                           meanFrameNorm[5,:],   # z
                           meanFrameNorm[6,:]])  # noise          
                           ]
        return data
    
    
    N = 24
    theta = radar_factory(N, frame='polygon')
    
    #data = example_data()
    data = example_data_normalized()
    spoke_labels = data.pop(0)
    
    ###fig, axes = plt.subplots(figsize=(9, 9), nrows=1, ncols=1, subplot_kw=dict(projection='radar'))
    fig, axes = plt.subplots(figsize=(7, 20), nrows=3, ncols=1,
                             subplot_kw=dict(projection='radar'))
    fig.subplots_adjust(wspace=0.25, hspace=0.25, top=0.9, bottom=0.05)
    
    colors = [['b', 'm','k'],
              ['r', 'y','k'],
              ['g', 'c','k']]
    i_c=-1
    i_l=-1
    # Plot the four cases from the example data on separate axes
    for ax, (title, case_data) in zip(axes.flatten(), data):
        i_c+=1
        ax.set_rgrids([0.2, 0.4, 0.6, 0.8])
        ax.set_title(title, weight='bold', size='medium', position=(0.5, 1.1),
                     horizontalalignment='center', verticalalignment='center')
        for d, color in zip(case_data, colors[i_c]):
            i_l+=1
            ax.plot(theta, d, color=color)
            p0,=ax.fill(theta, d, facecolor=color, alpha=0.5)
            if i_l==0: p1=p0
            if i_l==1: p2=p0
            if i_l==3: p3=p0
            if i_l==4: p4=p0
            if i_l==6: p5=p0
            if i_l==7: p6=p0
            if i_l==8: p7=p0            
        ax.set_varlabels(spoke_labels)
        ax.set_ylim([0.,.8])
    
    # add legend relative to top-left plot
    ##ax = axes
    labels = ('a','x',  'b', 'y', 'c','z','noise')
    ax=axes[1]
    legend = ax.legend((p1,p2,p3,p4,p5,p6,p7),labels,bbox_to_anchor=(0.5, 0.,1., 1.), loc='center right', fontsize='medium')
           
    fig.text(0.5, 0.95, titleNames[i_file],
                 horizontalalignment='center', color='black', weight='bold',
                 size='large')
    
    
    #plt.show()
    plt.savefig('radar_test_normed%s.png'%(titleNames[i_file]),  bbox_inches='tight' )
