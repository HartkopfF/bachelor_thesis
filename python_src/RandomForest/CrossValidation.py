'''
Created on 05.07.2017

@author: HartkopfF
'''

import Input
import sys
import os
import pandas
import numpy
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn import svm

if __name__ == '__main__':
    pass


# Change directory
os.chdir("../..")
cwd = os.getcwd()
print(cwd)

# Logging
log = open("output/CrossValidation.log","w") 
log.write(cwd+ '\n') 


# Import peaks
file = sys.argv[1]
if os.path.exists(file):
    print(os.path.basename(file))
    log.write(os.path.basename(file)) 
peaks = Input.readTrainingsSet(file)
print(len(peaks), "peaks were imported.")
log.write(str(len(peaks)) + " peaks were imported."+ '\n') 



####################
# Data Preparation #
####################
# Create matrix out of all peaks
peaksMatrix = [peaks[0].getHeader()]
for peak in peaks[:1000]:
    row = peak.getAsRow()
    peaksMatrix = numpy.vstack([peaksMatrix, row])


# Create dataframe
df = pandas.DataFrame(peaksMatrix[1:], columns=peaksMatrix[0])
features = df.columns[2:]
ionClasses = numpy.array(["noise","c","a","b","z","x","y"])
ionClassFact = pandas.factorize(df['ionClass'])[0]
df['ionClassFact'] = ionClassFact


####################
# Cross Validation #
####################

print(df.shape, ionClassFact.shape)
log.write(str(df.shape)+ str(ionClassFact.shape)+ '\n') 

X_train, X_test, y_train, y_test = train_test_split(df[features], ionClassFact, test_size=0.4, random_state=0)
 
print(X_train.shape, y_train.shape)
log.write(str(X_train.shape)+ str(y_train.shape)+ '\n') 

print(X_test.shape, y_test.shape)
log.write(str(X_test.shape)+ str(y_test.shape)+ '\n') 

clf = svm.SVC(kernel='linear', C=1)
scores = cross_val_score(clf, df[features], ionClassFact, cv=5)
print(scores)
log.write(str(scores)+ '\n') 
                                              
print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
log.write("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2)+ '\n') 


log.close() 
