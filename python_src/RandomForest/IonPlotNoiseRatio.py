'''
Created on 30.07.2017

@author: fhartkopf
'''

import matplotlib.pyplot as plt
import numpy as np

def countIons(file):
    print("Reading...")

    a = 0
    b = 0
    c = 0
    x = 0
    y = 0
    z = 0
    noise = 0
    other = 0
    peakCount = 0
    print("Counting...")
    
    found = 0
    
    with open(file) as f:
        for line in f:
            lineSplit = line.split()
            if(found > 0):
                print(line)
                found-=1
            if(len(lineSplit)>2):
                ionClass = lineSplit[2]
                peakCount+=1
                if(ionClass== "a"):
                    a+=1
                elif(ionClass== "b"):
                    b+=1
                elif(ionClass== "c"):
                    c+=1    
                elif(ionClass== "x"):
                    x+=1    
                elif(ionClass== "y"):
                    y+=1    
                elif(ionClass== "z"):
                    z+=1    
                elif(ionClass== "noise"):
                    noise+=1 
                else:
                    other+=1
            
    print(a,b,c,x,y,z,noise,other)
    print(a/peakCount,b/peakCount,c/peakCount,x/peakCount,y/peakCount,z/peakCount,noise/peakCount,other/peakCount)
    print(a+b+c+x+y+z+noise+other,peakCount)
    results = [a,b,c,x,y,z,noise,other,peakCount]
    return results

def plotHistSingle(data, title, output): 

    N = len(data)-2
    data = data[:-2]
    ind = np.arange(N)  # the x locations for the groups
    width = 0.6       # the width of the bars
    
    fig, ax = plt.subplots(figsize=(20, 10))
    rects1 = ax.bar(ind, data, width, color=color0)  # @UnusedVariable
            
    # add some text for labels, title and axes ticks
    ax.set_ylabel('Frequency')
    ax.set_title(title)
    ax.set_xticks(ind + width / 2)
    ax.set_xticklabels(('a-ion', 'b-ion','c-ion', 'x-ion', 'y-ion', 'z-ion','noise'  ))
    
    #ax.legend((rects1[0]), ('unprocessed'), loc=2, prop={'size': 20})
    fig.savefig('hist_ionPlot_'+output+'.png')
    #plt.show()
    
    
########
# Main #
########

color0 = "red"
color1 = "forestgreen"

font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 22}

plt.rc('font', **font)

file = "../../output/randomforest/Pyro_Velos_200_false_classified.mgf"
title = "Ion distribution of training set Pyrococcus furiosus"
print(title)
output = "pf"
countTraining = countIons(file)

file = "../../output/randomforest/UPS1_12500amol_R1-t5ppm-tda1-inst1-e1_human_200_false_classified.mgf"
title = 'Ion distribution of training set UPS1 with human database'
print(title)
output = "human"
countTraining = countIons(file)


file = "../../output/randomforest/UPS1_12500amol_R1-t5ppm-tda1-inst1-e1_yeast_ups_200_false_classified.mgf"
title = 'Ion distribution of training set UPS1 with yeast with spiked-in UPS1 database'
print(title)
output = "ups1"
countTraining = countIons(file)

file = "../../output/randomforest/UPS1_12500amol_R1-t5ppm-tda1-inst1-e1_yeast_200_false_classified.mgf"
title = 'Ion distribution of training set UPS1 with yeast database'
print(title)
output = "yeast"
countTraining = countIons(file)

file = "../../output/randomforest/01709a_GH2-TUM_first_pool_114_01_01-DDA-1h-R1-CID_200_false_classified.mgf"
title = 'Ion distribution of training set synthetic CID'
print(title)
output = "CID"
countTraining = countIons(file)

file = "../../output/randomforest/01709a_GH2-TUM_first_pool_114_01_01-DDA-1h-R1-HCD_200_false_classified.mgf"
title = 'Ion distribution of training set synthetic HCD'
print(title)
output = "HCD"
countTraining = countIons(file)
