'''
Created on 30.06.2017

@author: hartkopff
'''

class Peak(object):
    '''
    Class to hold spectra data
    '''
    mz = 0.0
    intensity = 0.0 
    ionClass = ""
    charge = 0
    rank = 0
    intBasePeak = 0.0                   
    intTotalIonCurrent = 0.0             
    localRank = 0
    intLocalBasePeak = 0.0
    intLocalTotalIonCurrent = 0.0
    h2oLoss = 0.0
    ch4Loss = 0.0
    nh3Loss = 0.0
    coLoss = 0.0
    hLoss = 0.0
    h2oGain = 0.0
    ch4Gain = 0.0
    nh3Gain = 0.0
    coGain = 0.0
    hGain = 0.0
    pCharge = 0
    rtenSec = 0.0
    pepmass = 0.0
    pepmassMz = 0.0
    complIon = 0.0
    
    def __init__(self):
        '''
        Constructor
        '''
    
    def getAsRow(self):
        row = [ self.ionClass, self.charge, self.mz, self.intensity, self.rank, self.intBasePeak, self.intTotalIonCurrent, self.localRank, self.intLocalBasePeak, self.intLocalTotalIonCurrent, self.h2oLoss, self.ch4Loss, self.nh3Loss, self.coLoss, self.hLoss, self.h2oGain, self.ch4Gain, self.nh3Gain, self.coGain, self.hGain, self.pCharge, self.rtenSec, self.pepmass, self.pepmassMz, self.complIon ]
        return(row)
    
    def getHeader(self):
        header = ["ionClass", "charge", "mz", "intensity", "rank", "intBasePeak", "intTotalIonCurrent", "localRank", "intLocalBasePeak", "intLocalTotalIonCurrent", "h2oLoss", "ch4Loss", "nh3Loss", "coLoss", "hLoss", "h2oGain", "ch4Gain", "nh3Gain", "coGain", "hGain", "pCharge", "rtenSec", "pepmass", "pepmassMz", "complIon"]
        
        return(header)
        
        
        
        