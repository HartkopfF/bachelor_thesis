'''
Created on 12.07.2017

@author: HartkopfF
'''

import Input  # @UnresolvedImport
import os
import sys
import pandas
import numpy
from sklearn.ensemble import RandomForestClassifier


if __name__ == '__main__':
    pass




# Change directory
os.chdir("../..")
cwd = os.getcwd()
print(cwd)
fraction = float(sys.argv[1])
print(fraction)

# Import peaks
file = "res/spectra/UPS1_yeast/UPS1_12500amol_R1.mgf_training.mgf"
tbcFile = "res/spectra/UPS1/UPS1_12500amol_R1.mgf_tbc.mgf"
rootFileName = file.split("/")[-1][:-16]
outputFile = rootFileName + "_classified_"+str(fraction)+".mgf"
peaks = Input.readTrainingsSetDataframe(file)
print(len(peaks), "peaks were imported.")


 
# Create dataframe
df = peaks
print(df[:10])
features = df.columns[2:]
print(features)
ionClassFact, ionClassDict = pandas.factorize(df['ionClass'])
print(ionClassDict)
df['ionClassFact'] = ionClassFact
print(df['ionClass'])
print(ionClassFact)
 
  
#################
# Random Forest #
#################
# Fitting
clf = RandomForestClassifier(bootstrap=False, class_weight='balanced', criterion='gini',
                              max_features='sqrt', max_leaf_nodes=100,
                              min_weight_fraction_leaf=0.0, n_estimators=20, n_jobs=-1,
                              oob_score=False, random_state=0, verbose=1, warm_start=False)
df['is_train'] = numpy.random.uniform(0, 1, len(df)) <= fraction
train, predict = df[df['is_train']==True], df[df['is_train']==False]
 
# Balance training data
#train = Input.balanceValidationFast(train)
 
fit = clf.fit(train[features], train['ionClassFact'])
print(fit)

# Create classification dictionary and apply it
ionClassDict = dict(zip(range(7), ionClassDict))
print(ionClassDict)
preds = clf.predict(predict[features])
preds = numpy.vectorize(ionClassDict.get)(preds)

 
print(clf.predict(predict[features]))
print(clf.predict_proba(predict[features]))
print(preds)
   
    
print(predict['ionClass'].head())
   
# Create confusion matrix
print(pandas.crosstab(predict['ionClass'], preds, rownames=['Actual Ion Class'], colnames=['Predicted Ion Class']))
    
# View a list of the features and their importance scores
print(list(zip(df[features], clf.feature_importances_)))

# #####################
# # Predict test data #
# #####################
# 
# if not os.path.exists("output"):
#         os.makedirs("output")
# if not os.path.exists("output/randomforest"):
#         os.makedirs("output/randomforest")
#      
#  
# output = open("output/randomforest/"+outputFile, 'w')
# 
# 
# print("Start writing to output/randomforest/"+outputFile)
# header = ["ionClass", "charge", "mz", "intensity", "rank", "intBasePeak", "intTotalIonCurrent", "localRank", "intLocalBasePeak", "intLocalTotalIonCurrent", "h2oLoss", "ch4Loss", "nh3Loss", "coLoss", "hLoss", "h2oGain", "ch4Gain", "nh3Gain", "coGain", "hGain", "pCharge", "rtenSec", "pepmass", "pepmassMz", "complIon"]  
# tbcMatrix = [header]          
# process = 0
# with open(tbcFile) as Input:
#     for tbcline in Input:
#         if(tbcline[:3]=="END" and len(tbcMatrix)>1):
#             print("Starting classifier for spectrum")
#             tbcData = pandas.DataFrame(tbcMatrix[1:], columns=tbcMatrix[0])
#             
#             # Create classification dictionary and apply it
#             
#             preds = clf.predict(tbcData[features])
#             preds = numpy.vectorize(ionClassDict.get)(preds)
#             print(preds)
#             print(clf.predict_proba(tbcData[features]))
# 
#             # Rearrange columns
#             colnames = header#df.columns.tolist()
#             colnames = colnames[2:4]+colnames[0:2]+colnames[4:-1]
#             tbcData = tbcData[colnames]
#             
#             outputLine = tbcData.to_string(header=False, index=False, index_names=False).split('\n')
#             for index in range(len(tbcData)):
#                 row = outputLine[index]+"\n"
#                 row = ' '.join(row.split())+"\n"
#                 row = row.replace('?', preds[index], 1)
#                 output.write(row)
#             tbcMatrix = [header] 
#             output.write(tbcline)       
#        
#         elif(tbcline[0].isdigit()):
#             lineSplit = tbcline.split(" ")[:-1]
#             lineSplit = lineSplit[2:4]+lineSplit[0:2]+lineSplit[4:]
#             tbcMatrix = numpy.vstack([tbcMatrix, lineSplit])
#             print(process, "peaks processed.")
#             process = process+1
#             
#         elif(len(tbcMatrix)<1):
#             print("Empty spectrum deleted")    
#         else:
#             output.write(tbcline)
#  
# output.close()        