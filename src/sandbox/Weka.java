package sandbox;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Random;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.RandomForest;

import weka.core.Instances;

public class Weka {

	public static void main(String[] args) throws Exception {
		
		BufferedReader br = null;
		int numFolds = 10;
		br = new BufferedReader(new FileReader("res/weka/weather_train.arff"));

		Instances trainData = new Instances(br);
		System.out.println(trainData);

		trainData.setClassIndex(trainData.numAttributes() - 1);

		br.close();
		RandomForest rf = new RandomForest();
		System.out.println(rf);

		//rf.setNumTrees(100);

		rf.buildClassifier(trainData);
		System.out.println(rf);

		Evaluation evaluation = new Evaluation(trainData);
		System.out.println(evaluation.toSummaryString("\nResults\n======\n", true));

		evaluation.crossValidateModel(rf, trainData, numFolds, new Random(1));

		System.out.println(evaluation.toSummaryString("\nResults\n======\n", true));
		System.out.println(evaluation.toClassDetailsString());
		System.out.println(evaluation.predictions());
		System.out.println("Results For Class -1- ");
		System.out.println("Precision=  " + evaluation.precision(0));
		System.out.println("Recall=  " + evaluation.recall(0));
		System.out.println("F-measure=  " + evaluation.fMeasure(0));
		System.out.println("Results For Class -2- ");
		System.out.println("Precision=  " + evaluation.precision(1));
		System.out.println("Recall=  " + evaluation.recall(1));
		System.out.println("F-measure=  " + evaluation.fMeasure(1));
		

	}

}


