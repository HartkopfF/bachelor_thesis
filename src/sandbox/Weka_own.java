package sandbox;

import java.util.Random;

import weka.classifiers.Evaluation;
import weka.classifiers.trees.RandomForest;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class Weka_own {
	public static void main(String[] args) throws Exception {
		
		int numFolds = 10;

		
		DataSource trainSource = new DataSource("res/weka/weather_train.arff");
		Instances dataTrain = trainSource.getDataSet();
		if (dataTrain.classIndex() == -1) {
			dataTrain.setClassIndex(dataTrain.numAttributes() - 1);
		}
		
		DataSource dataSource = new DataSource("res/weka/weather_data.arff");
		Instances data = dataSource.getDataSet();
		if (data.classIndex() == -1) {
			data.setClassIndex(data.numAttributes() - 1);
		}

		
		// Train model
		RandomForest rf = new RandomForest();
		rf.buildClassifier(dataTrain);

		// this does the trick  
		double label = rf.classifyInstance(data.instance(0));
		data.instance(0).setClassValue(label);

		System.out.println("Instance: "+data.instance(0).toString());
		System.out.println("Class label: "+label);
		System.out.println("Negative: "+rf.getNumTrees());
		System.out.println("Negative: "+rf.getNumTrees());

		System.out.println(data.instance(0).stringValue(4));
		System.out.println(data.toSummaryString());
		
		Evaluation evaluation = new Evaluation(dataTrain);
		evaluation.crossValidateModel(rf, dataTrain, numFolds, new Random(1));
		System.out.println(evaluation.toSummaryString("\nResults\n======\n", true));
		System.out.println(evaluation.toClassDetailsString());
		System.out.println(evaluation.predictions());
		


		
	}


}

