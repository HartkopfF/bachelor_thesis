package plots;

import java.awt.Color;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NavigableSet;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.function.LineFunction2D;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.statistics.Regression;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.mapdb.BTreeMap;


import model.Spectrum;
import util.Pair;

public class LinePlots{

	public static JFreeChart plotFDRtoNumberPeaks(BTreeMap<Integer,Spectrum> spectra){

		// get data
		XYSeriesCollection dataset = new XYSeriesCollection();
		XYSeries series = new XYSeries("FDR_NumberPeaks");

		NavigableSet<Integer> keys = spectra.getKeys();
		for(int key : keys){
			double fdr = spectra.get(key).msgfplusFDR;
			if(fdr>0){
				series.add(fdr, Math.log(spectra.get(key).peaks.size()));
			}
		}

		dataset.addSeries(series);

		JFreeChart chart = ChartFactory.createScatterPlot(
				"Scatter Plot of the FDR and the number of peaks", // chart title
				"FDR", // x axis label
				"#Peaks(log)", // y axis label
				dataset, // data  ***-----PROBLEM------***
				PlotOrientation.VERTICAL,
				true, // include legend
				true, // tooltips
				false // urls
				);

		// 5x5 red pixel circle
		Shape shape  = new Ellipse2D.Double(0,0,3,3);
		XYPlot plot = (XYPlot)chart.getPlot();
		XYItemRenderer renderer = plot.getRenderer();
		renderer.setBaseShape(shape);
		renderer.setBasePaint(Color.black);

		// set only shape of series with index i
		renderer.setSeriesShape(0, shape);

		//Changes background color
		plot.setBackgroundPaint(Color.white);

		// Get the parameters 'a' and 'b' for an equation y = a + b * x,
		// fitted to the inputData using ordinary least squares regression.
		// a - regressionParameters[0], b - regressionParameters[1]
		double regressionParameters[] = Regression.getOLSRegression( dataset, 0);

		// Prepare a line function using the found parameters
		LineFunction2D linefunction2d = new LineFunction2D(regressionParameters[0], regressionParameters[1]);

		// Creates a dataset by taking sample values from the line function
		XYDataset datasetRegression = DatasetUtilities.sampleFunction2D(linefunction2d,0.0,dataset.getSeries(0).getMaxX(), 100, "Fitted Regression Line");

		// Draw the line dataset
		plot.setDataset(1, datasetRegression);
		XYLineAndShapeRenderer xylineandshaperenderer = new XYLineAndShapeRenderer( true, false);
		xylineandshaperenderer.setSeriesPaint(0, Color.black);
		plot.setRenderer(1, xylineandshaperenderer);
		plot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);

		// create and display a frame...
		ChartFrame frame = new ChartFrame("Scatter plot of the FDR and the number of peaks", chart);
		frame.pack();
		frame.setVisible(true);

		return chart;
	}

	public static JFreeChart plotFDRtoMeanInt(BTreeMap<Integer,Spectrum> spectra){

		// get data
		XYSeriesCollection dataset = new XYSeriesCollection();
		XYSeries series = new XYSeries("FDR_MeanIntensity");

		NavigableSet<Integer> keys = spectra.getKeys();
		for(int key : keys){
			double fdr = spectra.get(key).msgfplusFDR;
			if(fdr>=0){
				List<Pair<Double,Double>> peaks =spectra.get(key).peaks;
				double numberPeaks = peaks.size();
				double sumInt = 0.0;
				for(Pair<Double,Double> peak : peaks){
					sumInt = sumInt + peak.getRight();
				}

				series.add(fdr, Math.log(sumInt/numberPeaks));
			}
		}

		dataset.addSeries(series);

		JFreeChart chart = ChartFactory.createScatterPlot(
				"Scatter plot of the FDR and mean intensity of spectra ", // chart title
				"FDR", // x axis label
				"Mean Intensity(log)", // y axis label
				dataset, // data  ***-----PROBLEM------***
				PlotOrientation.VERTICAL,
				true, // include legend
				true, // tooltips
				false // urls
				);

		// 5x5 red pixel circle
		Shape shape  = new Ellipse2D.Double(0,0,3,3);
		XYPlot plot = (XYPlot)chart.getPlot();
		XYItemRenderer renderer = plot.getRenderer();
		renderer.setBaseShape(shape);
		renderer.setBasePaint(Color.black);

		// set only shape of series with index i
		renderer.setSeriesShape(0, shape);

		//Changes background color
		plot.setBackgroundPaint(Color.white);

		// Get the parameters 'a' and 'b' for an equation y = a + b * x,
		// fitted to the inputData using ordinary least squares regression.
		// a - regressionParameters[0], b - regressionParameters[1]
		double regressionParameters[] = Regression.getOLSRegression( dataset, 0);

		// Prepare a line function using the found parameters
		LineFunction2D linefunction2d = new LineFunction2D(regressionParameters[0], regressionParameters[1]);

		// Creates a dataset by taking sample values from the line function
		XYDataset datasetRegression = DatasetUtilities.sampleFunction2D(linefunction2d,0.0,dataset.getSeries(0).getMaxX(), 100, "Fitted Regression Line");

		// Draw the line dataset
		plot.setDataset(1, datasetRegression);
		XYLineAndShapeRenderer xylineandshaperenderer = new XYLineAndShapeRenderer( true, false);
		xylineandshaperenderer.setSeriesPaint(0, Color.black);
		plot.setRenderer(1, xylineandshaperenderer);
		plot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);

		// create and display a frame...
		ChartFrame frame = new ChartFrame("Scatter plot of the FDR and mean intensity of spectra", chart);
		frame.pack();
		frame.setVisible(true);

		return chart;
	}

	public static JFreeChart plotFDRtoTop50Mean(BTreeMap<Integer,Spectrum> spectra){

		// get data
		XYSeriesCollection dataset = new XYSeriesCollection();
		XYSeries series = new XYSeries("FDR_Top50");

		NavigableSet<Integer> keys = spectra.getKeys();
		for(int key : keys){
			double fdr = spectra.get(key).msgfplusFDR;
			if(fdr>=0){
				List<Pair<Double,Double>> peaks =spectra.get(key).peaks;
				double numberPeaks = peaks.size();
				List<Double> list = new ArrayList<Double>();
				double sumInt = 0;
				for(Pair<Double,Double> peak : peaks){
					list.add(peak.getRight());
				}

				Collections.sort(list, Collections.reverseOrder());
				List<Double> top50;
				if(list.size()<50){
					top50 = list.subList(0, list.size());
				}else{
					top50 = list.subList(0, 50);
				}
				for(double peak : top50){
					sumInt = sumInt + peak;
				}
				series.add(fdr, Math.log(sumInt/numberPeaks));
			}
		}

		dataset.addSeries(series);

		JFreeChart chart = ChartFactory.createScatterPlot(
				"Scatter plot of the FDR and top 50 mean intensity of spectra ", // chart title
				"FDR", // x axis label
				"Mean Intensity(log)", // y axis label
				dataset, // data  ***-----PROBLEM------***
				PlotOrientation.VERTICAL,
				true, // include legend
				true, // tooltips
				false // urls
				);

		// 5x5 red pixel circle
		Shape shape  = new Ellipse2D.Double(0,0,3,3);
		XYPlot plot = (XYPlot)chart.getPlot();
		XYItemRenderer renderer = plot.getRenderer();
		renderer.setBaseShape(shape);
		renderer.setBasePaint(Color.black);

		// set only shape of series with index i
		renderer.setSeriesShape(0, shape);

		//Changes background color
		plot.setBackgroundPaint(Color.white);

		// Get the parameters 'a' and 'b' for an equation y = a + b * x,
		// fitted to the inputData using ordinary least squares regression.
		// a - regressionParameters[0], b - regressionParameters[1]
		double regressionParameters[] = Regression.getOLSRegression( dataset, 0);

		// Prepare a line function using the found parameters
		LineFunction2D linefunction2d = new LineFunction2D(regressionParameters[0], regressionParameters[1]);

		// Creates a dataset by taking sample values from the line function
		XYDataset datasetRegression = DatasetUtilities.sampleFunction2D(linefunction2d,0.0,dataset.getSeries(0).getMaxX(), 100, "Fitted Regression Line");

		// Draw the line dataset
		plot.setDataset(1, datasetRegression);
		XYLineAndShapeRenderer xylineandshaperenderer = new XYLineAndShapeRenderer( true, false);
		xylineandshaperenderer.setSeriesPaint(0, Color.black);
		plot.setRenderer(1, xylineandshaperenderer);
		plot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);

		// create and display a frame...
		ChartFrame frame = new ChartFrame("Scatter plot of the FDR and top 50 mean intensity of spectra", chart);
		frame.pack();
		frame.setVisible(true);

		return chart;
	}

	public static JFreeChart plotFDRtoTop5Mean(BTreeMap<Integer,Spectrum> spectra){

		// get data
		XYSeriesCollection dataset = new XYSeriesCollection();
		XYSeries series = new XYSeries("FDR_Top5");

		NavigableSet<Integer> keys = spectra.getKeys();
		for(int key : keys){
			double fdr = spectra.get(key).msgfplusFDR;
			if(fdr>=0){
				List<Pair<Double,Double>> peaks =spectra.get(key).peaks;
				double numberPeaks = peaks.size();
				List<Double> list = new ArrayList<Double>();
				double sumInt = 0;
				for(Pair<Double,Double> peak : peaks){
					list.add(peak.getRight());
				}

				Collections.sort(list, Collections.reverseOrder());
				List<Double> top5 = list.subList(0, 5);
				for(double peak : top5){
					sumInt = sumInt + peak;
				}
				series.add(fdr, Math.log(sumInt/numberPeaks));
			}
		}

		dataset.addSeries(series);

		JFreeChart chart = ChartFactory.createScatterPlot(
				"Scatter plot of the FDR and top 5 mean intensity of spectra ", // chart title
				"FDR", // x axis label
				"Mean Intensity(log)", // y axis label
				dataset, // data  ***-----PROBLEM------***
				PlotOrientation.VERTICAL,
				true, // include legend
				true, // tooltips
				false // urls
				);

		// 5x5 red pixel circle
		Shape shape  = new Ellipse2D.Double(0,0,3,3);
		XYPlot plot = (XYPlot)chart.getPlot();
		XYItemRenderer renderer = plot.getRenderer();
		renderer.setBaseShape(shape);
		renderer.setBasePaint(Color.black);

		// set only shape of series with index i
		renderer.setSeriesShape(0, shape);

		//Changes background color
		plot.setBackgroundPaint(Color.white);

		// Get the parameters 'a' and 'b' for an equation y = a + b * x,
		// fitted to the inputData using ordinary least squares regression.
		// a - regressionParameters[0], b - regressionParameters[1]
		double regressionParameters[] = Regression.getOLSRegression( dataset, 0);

		// Prepare a line function using the found parameters
		LineFunction2D linefunction2d = new LineFunction2D(regressionParameters[0], regressionParameters[1]);

		// Creates a dataset by taking sample values from the line function
		XYDataset datasetRegression = DatasetUtilities.sampleFunction2D(linefunction2d,0.0,dataset.getSeries(0).getMaxX(), 100, "Fitted Regression Line");

		// Draw the line dataset
		plot.setDataset(1, datasetRegression);
		XYLineAndShapeRenderer xylineandshaperenderer = new XYLineAndShapeRenderer( true, false);
		xylineandshaperenderer.setSeriesPaint(0, Color.black);
		plot.setRenderer(1, xylineandshaperenderer);
		plot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);

		// create and display a frame...
		ChartFrame frame = new ChartFrame("Scatter plot of the FDR and top 5 mean intensity of spectra", chart);
		frame.pack();
		frame.setVisible(true);

		return chart;
	}
	
	public static JFreeChart plotFDRtoPrecursor(BTreeMap<Integer,Spectrum> spectra){

		// get data
		XYSeriesCollection dataset = new XYSeriesCollection();
		XYSeries series = new XYSeries("FDR_Precursor");

		NavigableSet<Integer> keys = spectra.getKeys();
		for(int key : keys){
			double fdr = spectra.get(key).msgfplusFDR;
			if(fdr>=0){
				series.add(fdr, Math.log(spectra.get(key).pepmass));
			}
		}

		dataset.addSeries(series);

		JFreeChart chart = ChartFactory.createScatterPlot(
				"Scatter Plot of the FDR and precursor mass of spectra", // chart title
				"FDR", // x axis label
				"Precursor Mass(log)", // y axis label
				dataset, // data  ***-----PROBLEM------***
				PlotOrientation.VERTICAL,
				true, // include legend
				true, // tooltips
				false // urls
				);

		// 5x5 red pixel circle
		Shape shape  = new Ellipse2D.Double(0,0,3,3);
		XYPlot plot = (XYPlot)chart.getPlot();
		XYItemRenderer renderer = plot.getRenderer();
		renderer.setBaseShape(shape);
		renderer.setBasePaint(Color.black);

		// set only shape of series with index i
		renderer.setSeriesShape(0, shape);

		//Changes background color
		plot.setBackgroundPaint(Color.white);

		// Get the parameters 'a' and 'b' for an equation y = a + b * x,
		// fitted to the inputData using ordinary least squares regression.
		// a - regressionParameters[0], b - regressionParameters[1]
		double regressionParameters[] = Regression.getOLSRegression( dataset, 0);

		// Prepare a line function using the found parameters
		LineFunction2D linefunction2d = new LineFunction2D(regressionParameters[0], regressionParameters[1]);

		// Creates a dataset by taking sample values from the line function
		XYDataset datasetRegression = DatasetUtilities.sampleFunction2D(linefunction2d,0.0,dataset.getSeries(0).getMaxX(), 100, "Fitted Regression Line");

		// Draw the line dataset
		plot.setDataset(1, datasetRegression);
		XYLineAndShapeRenderer xylineandshaperenderer = new XYLineAndShapeRenderer( true, false);
		xylineandshaperenderer.setSeriesPaint(0, Color.black);
		plot.setRenderer(1, xylineandshaperenderer);
		plot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);

		// create and display a frame...
		ChartFrame frame = new ChartFrame("Scatter Plot of the FDR and precursor mass of spectra", chart);
		frame.pack();
		frame.setVisible(true);

		return chart;
	}

	public static JFreeChart plotFDRtoPrecursorInt(BTreeMap<Integer,Spectrum> spectra){

		// get data
		XYSeriesCollection dataset = new XYSeriesCollection();
		XYSeries series = new XYSeries("FDR_PrecursorInt");

		NavigableSet<Integer> keys = spectra.getKeys();
		for(int key : keys){
			double fdr = spectra.get(key).msgfplusFDR;
			if(fdr>=0){
				series.add(fdr, spectra.get(key).pepmassInt);
			}
		}

		dataset.addSeries(series);

		JFreeChart chart = ChartFactory.createScatterPlot(
				"Scatter Plot of the FDR and precursor mass intensity of spectra", // chart title
				"FDR", // x axis label
				"Precursor Mass Intensity", // y axis label
				dataset, // data  ***-----PROBLEM------***
				PlotOrientation.VERTICAL,
				true, // include legend
				true, // tooltips
				false // urls
				);

		// 5x5 red pixel circle
		Shape shape  = new Ellipse2D.Double(0,0,3,3);
		XYPlot plot = (XYPlot)chart.getPlot();
		XYItemRenderer renderer = plot.getRenderer();
		renderer.setBaseShape(shape);
		renderer.setBasePaint(Color.black);

		// set only shape of series with index i
		renderer.setSeriesShape(0, shape);

		//Changes background color
		plot.setBackgroundPaint(Color.white);

		// Get the parameters 'a' and 'b' for an equation y = a + b * x,
		// fitted to the inputData using ordinary least squares regression.
		// a - regressionParameters[0], b - regressionParameters[1]
		double regressionParameters[] = Regression.getOLSRegression( dataset, 0);

		// Prepare a line function using the found parameters
		LineFunction2D linefunction2d = new LineFunction2D(regressionParameters[0], regressionParameters[1]);

		// Creates a dataset by taking sample values from the line function
		XYDataset datasetRegression = DatasetUtilities.sampleFunction2D(linefunction2d,0.0,dataset.getSeries(0).getMaxX(), 100, "Fitted Regression Line");

		// Draw the line dataset
		plot.setDataset(1, datasetRegression);
		XYLineAndShapeRenderer xylineandshaperenderer = new XYLineAndShapeRenderer( true, false);
		xylineandshaperenderer.setSeriesPaint(0, Color.black);
		plot.setRenderer(1, xylineandshaperenderer);
		plot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);

		// create and display a frame...
		ChartFrame frame = new ChartFrame("Scatter plot of the FDR and precursor mass intensity of spectra", chart);
		frame.pack();
		frame.setVisible(true);

		return chart;
	}
	
	public static JFreeChart plotFDRtoRtintime(BTreeMap<Integer,Spectrum> spectra){

		// get data
		XYSeriesCollection dataset = new XYSeriesCollection();
		XYSeries series = new XYSeries("FDR_rtinseconds");

		NavigableSet<Integer> keys = spectra.getKeys();
		for(int key : keys){
			double fdr = spectra.get(key).msgfplusFDR;
			if(fdr>=0){
				series.add(fdr, Math.log(spectra.get(key).rtinseconds));
			}
		}

		dataset.addSeries(series);

		JFreeChart chart = ChartFactory.createScatterPlot(
				"Scatter plot of the FDR and retension time of spectra", // chart title
				"FDR", // x axis label
				"Retension Time(log)", // y axis label
				dataset, // data  ***-----PROBLEM------***
				PlotOrientation.VERTICAL,
				true, // include legend
				true, // tooltips
				false // urls
				);

		// 5x5 red pixel circle
		Shape shape  = new Ellipse2D.Double(0,0,3,3);
		XYPlot plot = (XYPlot)chart.getPlot();
		XYItemRenderer renderer = plot.getRenderer();
		renderer.setBaseShape(shape);
		renderer.setBasePaint(Color.black);

		// set only shape of series with index i
		renderer.setSeriesShape(0, shape);

		//Changes background color
		plot.setBackgroundPaint(Color.white);

		// Get the parameters 'a' and 'b' for an equation y = a + b * x,
		// fitted to the inputData using ordinary least squares regression.
		// a - regressionParameters[0], b - regressionParameters[1]
		double regressionParameters[] = Regression.getOLSRegression( dataset, 0);

		// Prepare a line function using the found parameters
		LineFunction2D linefunction2d = new LineFunction2D(regressionParameters[0], regressionParameters[1]);

		// Creates a dataset by taking sample values from the line function
		XYDataset datasetRegression = DatasetUtilities.sampleFunction2D(linefunction2d,0.0,dataset.getSeries(0).getMaxX(), 100, "Fitted Regression Line");

		// Draw the line dataset
		plot.setDataset(1, datasetRegression);
		XYLineAndShapeRenderer xylineandshaperenderer = new XYLineAndShapeRenderer( true, false);
		xylineandshaperenderer.setSeriesPaint(0, Color.black);
		plot.setRenderer(1, xylineandshaperenderer);
		plot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);

		// create and display a frame...
		ChartFrame frame = new ChartFrame("Scatter plot of the FDR and retension time of spectra", chart);
		frame.pack();
		frame.setVisible(true);

		return chart;
	}


}