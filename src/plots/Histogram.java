package plots;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.NavigableSet;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.statistics.HistogramType;
import org.mapdb.BTreeMap;

import model.Spectrum;

/**
 * Class to plot a histogram with jfreechart.
 * @author Felix Hartkopf
 *
 */
public class Histogram {

	/**
	 * Method to plot histograms.
	 * @param data Array of data that should be plotted in a histogram.
	 * @return Histogram
	 * @author Felix Hartkopf
	 */
	public static JFreeChart createHistogram(double[] data, String title,String xAxis){

		HistogramDataset dataset = new HistogramDataset();
		dataset.setType(HistogramType.FREQUENCY);
		dataset.addSeries("Hist",data,500); // Number of bins is 50
		String plotTitle = title;
		String yAxis = "Frequency";
		PlotOrientation orientation = PlotOrientation.VERTICAL;

		boolean show = false;
		boolean toolTips = false;
		boolean urls = false;
		JFreeChart chart = ChartFactory.createHistogram(plotTitle, xAxis, yAxis,
				dataset, orientation, show, toolTips, urls);

		chart.setBackgroundPaint(Color.white);
		
		ChartFrame frame = new ChartFrame("Histogram", chart);
		frame.pack();
		frame.setVisible(true);
		
		return chart;
	}
	
	/**
	 * Method to plot histograms.
	 * @param data Array of data that should be plotted in a histogram.
	 * @return Histogram
	 * @author Felix Hartkopf
	 * @param NavigableSet<Integer> keys 
	 */
	public static JFreeChart plotFDR(BTreeMap<Integer,Spectrum> spectra){
		List<Double> dataList = new ArrayList<Double>();
		
		NavigableSet<Integer> keys = spectra.getKeys();
		for(int key : keys){
			double fdr = spectra.get(key).msgfplusFDR;
			if(fdr>=0){
				dataList.add(fdr);
			}
		}
		
		double[] data = new double[dataList.size()];
		for(int i = 0; i < dataList.size(); i++){
			data[i] = dataList.get(i);
		}
		
		HistogramDataset dataset = new HistogramDataset();
		dataset.setType(HistogramType.FREQUENCY);
		dataset.addSeries("Hist",data,200); // Number of bins is 50
		String plotTitle = "Histogram";
		String xAxis = "Frequency";
		String yAxis = "FDR";
		PlotOrientation orientation = PlotOrientation.VERTICAL;

		boolean show = false;
		boolean toolTips = false;
		boolean urls = false;
		JFreeChart chart = ChartFactory.createHistogram(plotTitle, yAxis, xAxis,
				dataset, orientation, show, toolTips, urls);

		chart.setBackgroundPaint(Color.white);
		XYPlot plot =    (XYPlot)  chart.getPlot();  
		XYBarRenderer barRndr = (XYBarRenderer) plot.getRenderer();
		BarRenderer.setDefaultBarPainter(new StandardBarPainter());
		barRndr.setSeriesPaint(0, Color.red );  
		barRndr.setShadowVisible(false);
		plot.setRenderer( barRndr);
		
		// customize background
	    plot.setBackgroundPaint(Color.white);
	    plot.setDomainGridlinePaint(Color.white);
	    plot.setRangeGridlinePaint(Color.white);
	    plot.setDomainCrosshairVisible(true);
	    plot.setRangeCrosshairVisible(true);
		
		ChartFrame frame = new ChartFrame("Histogram", chart);
		frame.pack();
		frame.setVisible(true);
		
		return chart;
	}
}