package modules;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.OutputStreamWriter;


import util.ProgressBar;

public class Finalize {
	public static void removeNoise(String mgfFile) throws IOException{
				
		System.out.println("\nStarting input of MS/MS spectra...\n");

		// Count lines of file
		LineNumberReader  lnr = new LineNumberReader(new FileReader(new File(mgfFile)));
		lnr.skip(Long.MAX_VALUE);
		int lines = lnr.getLineNumber() + 1; //Add 1 because line index starts at 0
		lnr.close();

		// Progress bar
		long progress = 0;
		long startTimeInput = System.currentTimeMillis();
		System.out.println("\nRemoving noise from MS/MS spectra...\n");
		try(BufferedReader br = new BufferedReader(new FileReader(mgfFile))) {
			String line = br.readLine();
			progress++;
			
			//Export
			File fout = new File(mgfFile+"processed.mgf");
			FileOutputStream out = new FileOutputStream(fout);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));
			
			while (line != null) {
				
				if(line.length()>0&&Character.isDigit(line.charAt(0))){
					String[] lineSplit = line.split(" ");
					if(lineSplit[2].equals("noise")) {

						// Write nothing to file 
						// System.out.println("\n"+line);

						
						
					}else {						
						bw.write(lineSplit[0]+" "+lineSplit[1]);
						bw.newLine();
					}

				}else {
					if(line.charAt(0)=='C'){
						bw.write(line+"+");
					}else{
						bw.write(line);
					}
					bw.newLine();
				}
				line = br.readLine();
				progress++;
				ProgressBar.printProgress(startTimeInput, lines, progress);
			}	
			System.out.println("\nRemoved all noise from spectra");
			bw.close();

		}
	}
}
