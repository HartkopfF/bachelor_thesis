package modules;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.univocity.parsers.tsv.TsvParser;
import com.univocity.parsers.tsv.TsvParserSettings;

import model.PSM;

public class Synthetic {


	public static List<PSM> readSyntheticIDs(String idPath) throws FileNotFoundException {

		System.out.println("Start input for file "+idPath+"...");

		TsvParserSettings settings = new TsvParserSettings();
		settings.setHeaderExtractionEnabled(true);
		TsvParser parser = new TsvParser(settings);

		// Read ProteomeTools msms file
		List<String[]> allRows = parser.parseAll(new FileReader(idPath));

		List<PSM> psms = new ArrayList<PSM>();

		for(String[] row : allRows) {
			int id = Integer.parseInt(row[1]);
			String title = row[0];
			double precursor = Double.parseDouble(row[18]);
			int charge = Integer.parseInt(row[12]);
			String peptide = row[3];
			float score = Float.parseFloat(row[24]); 

			PSM psm = new PSM(id, title, precursor, charge, peptide, score);
			psms.add(psm);

		}
		System.out.println("Read "+psms.size()+" spectra details.");

		return psms;
	}
	
	public static Map<Integer, String> readSyntheticIDsAsMap(String idPath) throws FileNotFoundException {

		System.out.println("Start input for file "+idPath+"...");

		TsvParserSettings settings = new TsvParserSettings();
		settings.setHeaderExtractionEnabled(true);
		TsvParser parser = new TsvParser(settings);

		// Read ProteomeTools msms file
		List<String[]> allRows = parser.parseAll(new FileReader(idPath));

		Map<Integer,String> psms = new HashMap<Integer,String>();

		for(String[] row : allRows) {
			int id = Integer.parseInt(row[1]);
			//String title = row[0];
			//double precursor = Double.parseDouble(row[18]);
			//int charge = Integer.parseInt(row[12]);
			String peptide = row[3];
			//float score = Float.parseFloat(row[24]); 

			//PSM psm = new PSM(id, title, precursor, charge, peptide, score);
			psms.put(id,peptide);

		}
		System.out.println("Read "+psms.size()+" spectra details.");

		return psms;
	}
}
