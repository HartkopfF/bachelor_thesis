package modules;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NavigableSet;
import java.util.Properties;

import org.mapdb.BTreeMap;

import model.Masses;
import model.PSM;
import model.Peptide;
import model.Spectrum;
import util.Pair;
import util.ProgressBar;

public class Features {

	public Features() {
	}

	public List<Spectrum> run(BTreeMap<Integer,Spectrum> spectra, List<PSM> msgfPSMs) throws FileNotFoundException, IOException{

		System.out.println("\n#######################");
		System.out.println("# Load config file... #");
		System.out.println("#######################\n");

		// Read config file
		Properties config = new Properties();
		Reader file = new FileReader("config.config");
		config.load(file);

		// Parameters
		String spectraPath = config.getProperty("spectraPath");
		double tolerance = Double.parseDouble(config.getProperty("tolerance"));

		System.out.println("\n###################################");
		System.out.println("# Extracting trainings spectra... #");
		System.out.println("###################################\n");

		// Extract spiked-in spectra for training set
		List<Spectrum> trainSpectra = new ArrayList<Spectrum>();
		for(PSM tmpPSM : msgfPSMs){
			int id = tmpPSM.getScanID();
			Spectrum tmpSpectrum = spectra.get(id);
			tmpSpectrum.msgfplusFDR = tmpPSM.getQvalue();
			Peptide peptide = new Peptide(tmpPSM.getPeptide());
			tmpSpectrum.peptide = peptide;
			if(tmpPSM.getProteins()!=null) {
				tmpSpectrum.proteins = tmpPSM.getProteins();
			}
			tmpSpectrum.score = tmpPSM.getScore();
			trainSpectra.add(tmpSpectrum);
		}

		System.out.println("\n "+trainSpectra.size()+" spectra extracted.\n");

		System.out.println("\n###########################");
		System.out.println("# Fragmentation started... #");
		System.out.println("###########################\n");

		// Fragmentation and classification
		for(Spectrum tmpSpectrum : trainSpectra ) {
			tmpSpectrum.peptide.fragmentate();							
		}

		System.out.println("\nAll sequences are fragmentated.\n");

		System.out.println("\n#################################################");
		System.out.println("# Calculating features for trainings spectra... #");
		System.out.println("#################################################\n");

		// Export and Features for training spectra

		// Progress bar
		long progress = 1;
		long startTimeInput = System.currentTimeMillis();

		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(spectraPath+"_training.mgf"), "utf-8"))) {
			for(Spectrum tmpSpectrum : trainSpectra ) {
				ProgressBar.printProgress(startTimeInput, trainSpectra.size(), progress);
				progress++;
				List<Pair<Double, Double>> peaks = tmpSpectrum.peaks;
				writer.write("BEGIN IONS\n");
				writer.write("TITLE="+tmpSpectrum.title+" "+tmpSpectrum.peptide.sequence+"\n");
				writer.write("PEPMASS="+tmpSpectrum.pepmass+"\n");
				writer.write("CHARGE="+tmpSpectrum.charge+"\n");
				writer.write("RTINSECONDS="+tmpSpectrum.rtinseconds+"\n");
				writer.write("SCANS="+tmpSpectrum.scans+"\n");

				for(Pair<Double, Double> peak : peaks) {
					double mz = peak.getLeft();
					double intensity = peak.getRight();
					Peptide peptide = tmpSpectrum.peptide;
					String ionClass = peptide.classifyPeak(mz, tolerance);
					int charge = Integer.parseInt(ionClass.split(" ")[1]);
					ionClass = ionClass.split(" ")[0];


					writer.write(mz+" "+															// Mass-to-charge
							intensity+" "+															// Intensity
							ionClass +" "+															// Ion Class
							charge +" "+															// Charge
							getRank(tmpSpectrum, peak) +" "+										// Global rank
							intensity/getBasePeak(tmpSpectrum).getRight()+" "+						// Intensity / base peak intensity
							intensity/getTotalIonCurrent(tmpSpectrum)+" "+							// Intensity / total ion current
							getLocalRank(tmpSpectrum, peak,  56.0)+" "+								// Local rank
							intensity/getLocalBasePeak(tmpSpectrum, peak,  56.0).getRight()+" "+	// Intensity / local base peak intensity 
							intensity/getLocalTotalIonCurrent(tmpSpectrum, peak,  56.0)+" "+		// Intensity / local total ion current
							getLossIonInt(tmpSpectrum, peak, 18.02, charge, tolerance, true)+" "+	// H2O loss
							getLossIonInt(tmpSpectrum, peak, 16.04, charge, tolerance, true)+" "+	// CH4 loss
							getLossIonInt(tmpSpectrum, peak, 17.03, charge, tolerance, true)+" "+	// NH3 loss
							getLossIonInt(tmpSpectrum, peak, 28.01, charge, tolerance, true)+" "+	// CO loss
							getLossIonInt(tmpSpectrum, peak, 1.01, charge, tolerance, true)+" "+	// H loss
							getLossIonInt(tmpSpectrum, peak, 18.02, charge, tolerance, false)+" "+	// H2O gain
							getLossIonInt(tmpSpectrum, peak, 16.04, charge, tolerance, false)+" "+	// CH4 gain
							getLossIonInt(tmpSpectrum, peak, 17.03, charge, tolerance, false)+" "+	// NH3 gain
							getLossIonInt(tmpSpectrum, peak, 28.01, charge, tolerance, false)+" "+	// CO gain
							getLossIonInt(tmpSpectrum, peak, 1.01, charge, tolerance, false)+" "+	// H gain
							tmpSpectrum.charge+" "+													// Precursor charge
							tmpSpectrum.rtinseconds+" "+											// Retention time
							tmpSpectrum.pepmass+" "+												// Precursor mass
							tmpSpectrum.pepmass/mz+" "+												// Precursor mass / m/z peak
							getComplementaryIon(tmpSpectrum, peak, tmpSpectrum.pepmass, tolerance)+" "			// complementary ion



							);
					writer.write("\n");


				}
				writer.write("END IONS\n");

			}
		}

		System.out.println("\n###############################################");
		System.out.println("# Calculating features for unknown spectra... #");
		System.out.println("###############################################\n");

		// Export and Features for to be classified spectra

		// Progress bar
		progress = 1;
		startTimeInput = System.currentTimeMillis();

		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(spectraPath+"_tbc.mgf"), "utf-8"))) {
			NavigableSet<Integer> keys = spectra.getKeys();
			for(Integer key : keys ) {
				ProgressBar.printProgress(startTimeInput, spectra.size(), progress);
				progress++;
				Spectrum tmpSpectrum = spectra.get(key);
				List<Pair<Double, Double>> peaks = tmpSpectrum.peaks;
				writer.write("BEGIN IONS\n");
				writer.write("TITLE="+tmpSpectrum.title+"\n");
				writer.write("PEPMASS="+tmpSpectrum.pepmass+"\n");
				writer.write("CHARGE="+tmpSpectrum.charge+"\n");
				writer.write("RTINSECONDS="+tmpSpectrum.rtinseconds+"\n");
				writer.write("SCANS="+tmpSpectrum.scans+"\n");

				for(Pair<Double, Double> peak : peaks) {
					double mz = peak.getLeft();
					double intensity = peak.getRight();

					// Loss ions
					List<Double> h2oList = new ArrayList<Double>(); 
					h2oList.add(getLossIonInt(tmpSpectrum, peak, 18.02, 1, tolerance, true));
					h2oList.add(getLossIonInt(tmpSpectrum, peak, 18.02, 2, tolerance, true));
					h2oList.add(getLossIonInt(tmpSpectrum, peak, 18.02, 3, tolerance, true));

					List<Double> ch4List = new ArrayList<Double>(); 
					ch4List.add(getLossIonInt(tmpSpectrum, peak, 16.04, 1, tolerance, true));
					ch4List.add(getLossIonInt(tmpSpectrum, peak, 16.04, 2, tolerance, true));
					ch4List.add(getLossIonInt(tmpSpectrum, peak, 16.04, 3, tolerance, true));

					List<Double> nh3List = new ArrayList<Double>(); 
					nh3List.add(getLossIonInt(tmpSpectrum, peak, 17.03, 1, tolerance, true));
					nh3List.add(getLossIonInt(tmpSpectrum, peak, 17.03, 2, tolerance, true));
					nh3List.add(getLossIonInt(tmpSpectrum, peak, 17.03, 3, tolerance, true));

					List<Double> coList = new ArrayList<Double>(); 
					coList.add(getLossIonInt(tmpSpectrum, peak, 28.01, 1, tolerance, true));
					coList.add(getLossIonInt(tmpSpectrum, peak, 28.01, 2, tolerance, true));
					coList.add(getLossIonInt(tmpSpectrum, peak, 28.01, 3, tolerance, true));

					List<Double> hList = new ArrayList<Double>(); 
					hList.add(getLossIonInt(tmpSpectrum, peak, 1.01, 1, tolerance, true));
					hList.add(getLossIonInt(tmpSpectrum, peak, 1.01, 2, tolerance, true));
					hList.add(getLossIonInt(tmpSpectrum, peak, 1.01, 3, tolerance, true));

					// Gain ions
					List<Double> h2oGainList = new ArrayList<Double>(); 
					h2oGainList.add(getLossIonInt(tmpSpectrum, peak, 18.02, 1, tolerance, false));
					h2oGainList.add(getLossIonInt(tmpSpectrum, peak, 18.02, 2, tolerance, false));
					h2oGainList.add(getLossIonInt(tmpSpectrum, peak, 18.02, 3, tolerance, false));

					List<Double> ch4GainList = new ArrayList<Double>(); 
					ch4GainList.add(getLossIonInt(tmpSpectrum, peak, 16.04, 1, tolerance, false));
					ch4GainList.add(getLossIonInt(tmpSpectrum, peak, 16.04, 2, tolerance, false));
					ch4GainList.add(getLossIonInt(tmpSpectrum, peak, 16.04, 3, tolerance, false));

					List<Double> nh3GainList = new ArrayList<Double>(); 
					nh3GainList.add(getLossIonInt(tmpSpectrum, peak, 17.03, 1, tolerance, false));
					nh3GainList.add(getLossIonInt(tmpSpectrum, peak, 17.03, 2, tolerance, false));
					nh3GainList.add(getLossIonInt(tmpSpectrum, peak, 17.03, 3, tolerance, false));

					List<Double> coGainList = new ArrayList<Double>(); 
					coGainList.add(getLossIonInt(tmpSpectrum, peak, 28.01, 1, tolerance, false));
					coGainList.add(getLossIonInt(tmpSpectrum, peak, 28.01, 2, tolerance, false));
					coGainList.add(getLossIonInt(tmpSpectrum, peak, 28.01, 3, tolerance, false));

					List<Double> hGainList = new ArrayList<Double>(); 
					hGainList.add(getLossIonInt(tmpSpectrum, peak, 1.01, 1, tolerance, false));
					hGainList.add(getLossIonInt(tmpSpectrum, peak, 1.01, 2, tolerance, false));
					hGainList.add(getLossIonInt(tmpSpectrum, peak, 1.01, 3, tolerance, false));

					writer.write(mz+" "+															// Mass-to-charge
							intensity+" "+															// Intensity
							"?" +" "+																// Ion Class
							"?" +" "+																// Charge
							getRank(tmpSpectrum, peak) +" "+										// Global rank
							intensity/getBasePeak(tmpSpectrum).getRight()+" "+						// Intensity / base peak intensity
							intensity/getTotalIonCurrent(tmpSpectrum)+" "+							// Intensity / total ion current
							getLocalRank(tmpSpectrum, peak,  56.0)+" "+								// Local rank
							intensity/getLocalBasePeak(tmpSpectrum, peak,  56.0).getRight()+" "+	// Intensity / local base peak intensity 
							intensity/getLocalTotalIonCurrent(tmpSpectrum, peak,  56.0)+" "+		// Intensity / local total ion current
							Collections.max(h2oList)+" "+											// H2O loss
							Collections.max(ch4List)+" "+											// CH4 loss
							Collections.max(nh3List)+" "+											// NH3 loss
							Collections.max(coList)+" "+											// CO loss
							Collections.max(hList)+" "+												// H loss
							Collections.max(h2oGainList)+" "+										// H2O gain
							Collections.max(ch4GainList)+" "+										// CH4 gain
							Collections.max(nh3GainList)+" "+										// NH3 gain
							Collections.max(coGainList)+" "+										// CO gain
							Collections.max(hGainList)+" "+											// H gain
							tmpSpectrum.charge+" "+													// Precursor charge
							tmpSpectrum.rtinseconds+" "+											// Retention time
							tmpSpectrum.pepmass+" "+												// Precursor mass
							tmpSpectrum.pepmass/mz+" "+												// Precursor mass / m/z peak
							getComplementaryIon(tmpSpectrum, peak, tmpSpectrum.pepmass,tolerance)+" "			// complementary ion

							);
					writer.write("\n");


				}
				writer.write("END IONS\n");

			}
		}

		return trainSpectra;

	}

	public int getRank(Spectrum spectrum, Pair<Double, Double> peak) {

		// Order peaks
		List<Pair<Double, Double>> peaks = spectrum.peaks;
		int rank = 1;
		for(Pair<Double,Double> tmpPeak : peaks) {
			if(tmpPeak.getRight()>peak.getRight()) {
				rank++;
			}
		}		

		return rank;	
	}

	public static Pair<Double, Double> getBasePeak(Spectrum spectrum) {

		// Order peaks
		List<Pair<Double, Double>> peaks = spectrum.peaks;
		Pair<Double,Double> basePeak = peaks.get(0);
		for(Pair<Double,Double> tmpPeak : peaks) {
			if(tmpPeak.getRight()>basePeak.getRight()) {
				basePeak = tmpPeak;
			}
		}		

		return basePeak;	
	}

	public static Double getTotalIonCurrent(Spectrum spectrum) {

		List<Pair<Double, Double>> peaks = spectrum.peaks;
		Double tic = 0.0;
		for(Pair<Double,Double> tmpPeak : peaks) {
			tic = tic + tmpPeak.getRight();
		}		

		return tic;	
	}


	public int getLocalRank(Spectrum spectrum, Pair<Double, Double> peak, double width) {

		List<Pair<Double, Double>> peaks = spectrum.peaks;
		int rank = 1;
		for(Pair<Double,Double> tmpPeak : peaks) {
			if(tmpPeak.getLeft()>peak.getLeft()-width/2&&tmpPeak.getLeft()<peak.getLeft()+width/2) {
				if(tmpPeak.getRight()>peak.getRight()) {
					rank++;
				}
			}	
		}		

		return rank;	
	}

	public static Pair<Double, Double> getLocalBasePeak(Spectrum spectrum, Pair<Double, Double> peak, double width) {

		List<Pair<Double, Double>> peaks = spectrum.peaks;
		Pair<Double,Double> basePeak = peak;
		for(Pair<Double,Double> tmpPeak : peaks) {
			if(tmpPeak.getLeft()>peak.getLeft()-width/2&&tmpPeak.getLeft()<peak.getLeft()+width/2) {
				if(tmpPeak.getRight()>basePeak.getRight()) {
					basePeak = tmpPeak;
				}
			}			
		}		

		return basePeak;	
	}

	public static Double getLocalTotalIonCurrent(Spectrum spectrum, Pair<Double, Double> peak, double width) {

		List<Pair<Double, Double>> peaks = spectrum.peaks;
		Double tic = 0.0;
		for(Pair<Double,Double> tmpPeak : peaks) {
			if(tmpPeak.getLeft()>peak.getLeft()-width/2&&tmpPeak.getLeft()<peak.getLeft()+width/2) {
				tic = tic + tmpPeak.getRight();
			}
		}		

		return tic;	
	}

	public static Double getLossIonInt(Spectrum spectrum, Pair<Double, Double> peak, double massShift, int charge, double tolerance,boolean loss) {

		List<Pair<Double, Double>> peaks = spectrum.peaks;
		List<Double> peaksMZ = new ArrayList<Double>();
		List<Double> peaksInt = new ArrayList<Double>();
		double lossInt = 0.0;
		massShift = ((massShift+charge*Masses.Hydrogen)/charge)-Masses.Hydrogen;

		if (charge != 0) {
			for(Pair<Double, Double> tmpPeak : peaks) {
				peaksMZ.add(tmpPeak.getLeft());
				peaksInt.add(tmpPeak.getRight());
			}
			
			double absError = 100;
			Double closestMZ;
			if(loss){
				closestMZ = Peptide.binarySearch(peak.getLeft()-massShift, peaksMZ);
				absError = Math.abs(closestMZ + massShift - peak.getLeft());
			}else{
				closestMZ = Peptide.binarySearch(peak.getLeft()+massShift, peaksMZ);
				absError = Math.abs(closestMZ - massShift - peak.getLeft());
			}


			if(absError<tolerance) {

				lossInt = peaksInt.get(peaksMZ.indexOf(closestMZ));
			}
		}

		return lossInt;	
	}

	public static Double getComplementaryIon(Spectrum spectrum, Pair<Double, Double> peak, double pepmass, double tolerance) {
		double intensity = 0.0;

		List<Pair<Double, Double>> peaks = spectrum.peaks;
		List<Double> peaksMZ = new ArrayList<Double>();
		List<Double> peaksInt = new ArrayList<Double>();

		for(Pair<Double, Double> tmpPeak : peaks) {
			peaksMZ.add(tmpPeak.getLeft());
			peaksInt.add(tmpPeak.getRight());
		}

		Double closestMZ = Peptide.binarySearch(pepmass-peak.getLeft(), peaksMZ);

		Double absError = Math.abs(closestMZ + peak.getLeft() - pepmass );

		if(absError<tolerance) {
			intensity = peaksInt.get(peaksMZ.indexOf(closestMZ));
		}


		return intensity;
	}


}
