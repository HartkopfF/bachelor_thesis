package util;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import org.apache.commons.lang3.SystemUtils;


public class MSGFplusProcess extends Thread {
	
	///////////////
	// Variables //
	///////////////

	/**
     * The path for the database.
     */
    protected String databasePath = null;

    /**
     * This String holds the path of the Mascot Generic File.
     */
    protected String mgfPath = null;
    
    /**
     * This String holds the path of MSGFPlus.jar.
     */
    protected String mSGFplusPath = null;
    
    /**
     * This String holds the path of output.
     */
    protected String outputFile = null;
	
	/////////////////
	// Constructor //
	/////////////////
	
	/**
	 * Constructor of MSGFplus task
	 * @param databasePath
	 * @param mgfPath
	 * @param mSGFplusPath
	 * @param outputFile
	 * @author Felix Hartkopf
	 */
	public MSGFplusProcess(String databasePath, String mgfPath,String mSGFplusPath,String outputFile){
		this.databasePath = databasePath;
		this.mgfPath = mgfPath;
		this.mSGFplusPath = mSGFplusPath;
		this.outputFile = outputFile;
	}
	
	///////////////////
	// Class Methods //
	///////////////////

	/**
	 *  Method to start MSGFplus task
	 */
	public void run(){
		try {
			// Create output folder
			File dir = new File("output/MSGFplus/");
			dir.mkdirs();
			
			// Read config file
			Properties config = new Properties();
			Reader file = new FileReader("config.config");
			config.load(file);
							
			// MSGFplus
			String coresString = config.getProperty("cores");
			
			int cores;
			if(coresString == "all"){
				cores = Runtime.getRuntime().availableProcessors();
			}else{
				cores = Integer.parseInt(coresString);
			}
			
			
			Process p;
			if(SystemUtils.IS_OS_LINUX){
				Path currentRelativePath = Paths.get("");
				String s = currentRelativePath.toAbsolutePath().toString();
				System.out.println("Current relative path is: " + s);
				System.out.println("java -Xmx3500M -jar "+mSGFplusPath+"/MSGFPlus.jar -s "+mgfPath+" -d "+databasePath+" -thread "+cores+" -t 5ppm -tda 1 -inst 1 -e 1 -o output/MSGFplus/"+outputFile+".mzid");
				p = Runtime.getRuntime().exec(new String[]{"bash","-c","java -Xmx3500M -jar "+mSGFplusPath+"/MSGFPlus.jar -s "+mgfPath+" -d "+databasePath+" -thread "+cores+" -t 5ppm -tda 1 -inst 1 -e 1 -o output/MSGFplus/"+outputFile+".mzid"});
				System.out.println("Waiting for MSGFplus with "+mgfPath+" ...\n");
				p.waitFor();
				java.io.InputStream is=p.getInputStream();
			    byte b[]=new byte[is.available()];
			    is.read(b,0,b.length);
			    java.io.InputStream isError=p.getErrorStream();
			    byte bError[]=new byte[isError.available()];
			    is.read(bError,0,bError.length);
			    System.out.println(new String(b));
			    System.out.println(new String(bError));
			    System.out.println("MSGFplus done with "+mgfPath);
			    
			    currentRelativePath = Paths.get("");
			    s = currentRelativePath.toAbsolutePath().toString();
			    System.out.println("Current relative path is: " + s);
			    System.out.println("java -jar -Xmx3500M -cp "+mSGFplusPath+"/MSGFPlus.jar edu.ucsd.msjava.ui.MzIDToTsv -i output/MSGFplus/"+outputFile+".mzid -o output/MSGFplus/"+outputFile+".tsv");
				p = Runtime.getRuntime().exec(new String[]{"java -Xmx3500M -cp "+mSGFplusPath+"/MSGFPlus.jar edu.ucsd.msjava.ui.MzIDToTsv -i output/MSGFplus/"+outputFile+".mzid -o output/MSGFplus/"+outputFile+".tsv"});
			    System.out.println("Waiting for MzIDToTsv with "+mgfPath+" ...");
				p.waitFor();
				java.io.InputStream is2=p.getInputStream();
			    byte b2[]=new byte[is2.available()];
			    is2.read(b2,0,b2.length);
			    java.io.InputStream isError2=p.getErrorStream();
			    byte bError2[]=new byte[isError2.available()];
			    is.read(bError2,0,bError2.length);
			    System.out.println(new String(b2));
			    System.out.println(new String(bError2));
			    System.out.println("\nMzIDToTsv done with "+mgfPath);	
			    
			}else if(SystemUtils.IS_OS_WINDOWS){
				System.out.println("java -Xmx3500M -jar "+mSGFplusPath+"/MSGFPlus.jar -s "+mgfPath+" -d "+databasePath+" -thread "+cores+" -t 5ppm -tda 1 -inst 1 -e 1 -o output/MSGFplus/"+outputFile+".mzid");
				p = Runtime.getRuntime().exec("java -Xmx3500M -jar "+mSGFplusPath+"/MSGFPlus.jar -s "+mgfPath+" -d "+databasePath+" -thread "+cores+" -t 5ppm -tda 1 -inst 1 -e 1 -o output/MSGFplus/"+outputFile+".mzid");
				System.out.println("Waiting for MSGFplus with "+mgfPath+" ...\n");
				p.waitFor();
				java.io.InputStream is=p.getInputStream();
			    byte b[]=new byte[is.available()];
			    is.read(b,0,b.length);
			    java.io.InputStream isError=p.getErrorStream();
			    byte bError[]=new byte[isError.available()];
			    is.read(bError,0,bError.length);
			    System.out.println(new String(b));
			    System.out.println(new String(bError));
			    System.out.println("MSGFplus done with "+mgfPath);

			    
			    p = Runtime.getRuntime().exec("java -Xmx3500M -cp "+mSGFplusPath+"/MSGFPlus.jar edu.ucsd.msjava.ui.MzIDToTsv -i output/MSGFplus/"+outputFile+".mzid -o output/MSGFplus/"+outputFile+".tsv");
			    System.out.println("Waiting for MzIDToTsv with "+mgfPath+" ...");
				p.waitFor();
				java.io.InputStream is2=p.getInputStream();
			    byte b2[]=new byte[is2.available()];
			    is2.read(b2,0,b2.length);
			    java.io.InputStream isError2=p.getErrorStream();
			    byte bError2[]=new byte[isError2.available()];
			    is.read(bError2,0,bError2.length);
			    System.out.println(new String(b2));
			    System.out.println(new String(bError2));
			    System.out.println("\nMzIDToTsv done with "+mgfPath);		
			}
			
		    
		} 
		catch (IOException | InterruptedException e) {
			e.printStackTrace();

		}
	}
	
		
}
