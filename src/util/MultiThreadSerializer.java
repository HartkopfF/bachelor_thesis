package util;

import org.mapdb.BTreeMap;

import model.Spectrum;

public class MultiThreadSerializer implements Runnable {


	BTreeMap<Integer,Spectrum> primaryMap;
	int scans;
	Spectrum spectrum;

	public MultiThreadSerializer(BTreeMap<Integer,Spectrum> primaryMap,int scans, Spectrum spectrum){
		this.primaryMap = primaryMap;
		this.scans = scans;
		this.spectrum = spectrum;
	}

	@Override
	public void run() {
		System.out.println(Thread.currentThread().getName()+" Start. Command = "+scans);
		processCommand(primaryMap,scans, spectrum);
		System.out.println(Thread.currentThread().getName()+" End.");
	}

	private void processCommand(BTreeMap<Integer,Spectrum> primaryMap,int scans, Spectrum spectrum) {
		primaryMap.put(scans, spectrum);
	}
}
