package util;

import java.io.File;
import java.io.IOException;


public class NovorProcess extends Thread {
	
	///////////////
	// Variables //
	///////////////

	/**
     * The path for the parameter file.
     */
    protected String paramPath = null;

    /**
     * This String holds the path of the Mascot Generic File.
     */
    protected String mgfPath = null;
    
    /**
     * This String holds the path of novor.jar.
     */
    protected String novorPath = null;
    
    /**
     * This String holds the path of output.
     */
    protected String outputFile = null;
	
	/////////////////
	// Constructor //
	/////////////////
	
	/**
	 * Constructor of MSGFplus task
	 * @param mgfPath
	 * @param novorPath 
	 * @param outputFile
	 * @param paramPath
	 * @author Felix Hartkopf
	 */
	public NovorProcess(String mgfPath,String novorPath,String outputFile, String paramPath){
		this.mgfPath = mgfPath;
		this.novorPath = novorPath;
		this.outputFile = outputFile;
		this.paramPath = paramPath;
	}
	
	///////////////////
	// Class Methods //
	///////////////////

	/**
	 *  Method to start novor task
	 */
	public void run(){
		try {
			// Create output folder
			
			File dir = new File("output/Novor/");
			dir.mkdirs();
			
			// Start Novor process
			Process p;
	        System.out.println("java -Xmx3500M -jar "+novorPath+"/novor.jar -p "+paramPath+" "+mgfPath+" "+"-o output/Novor/"+outputFile+".csv -f");
			p = Runtime.getRuntime().exec("java -Xmx3500M -jar "+novorPath+"/novor.jar -p "+paramPath+" "+mgfPath+" "+"-o output/Novor/"+outputFile+".csv -f");
			System.out.println("Waiting for Novor with "+mgfPath+" ...");
			p.waitFor();
			java.io.InputStream is=p.getInputStream();
	        byte b[]=new byte[is.available()];
	        is.read(b,0,b.length);
	        System.out.println(new String(b));
		    System.out.println("Novor done with "+mgfPath);	
		   
		} 
		catch (IOException | InterruptedException e) {
			e.printStackTrace();

		}
	}
	
}
