package util;

import java.util.ArrayList;
import java.util.List;

public class Digest {
	
	////////////////////
	// Static Methods //
	////////////////////

	public static String[] performNaivTrypticCleavage(String sequence){
		String[] peptides = null;
		
		sequence = sequence.replaceAll("R", "R,");
		sequence = sequence.replaceAll("K", "K,");
		peptides = sequence.split(",");
		
		
		return peptides;
	}
	
	
	/**
	 * Perform tryptic cleavage on a AA sequence.
	 * @param proteinSequence The protein sequence
	 * @param missedCleavages The number of missed cleavages
	 * @return List of tryptic peptides.
	 * @author Thilo Muth
	 */
	public static List<String> performTrypticCleavage(String proteinSequence, int minLength, int maxLength,int missedCleavages) {
		
		// Cleavage rules for tryptic digestion
		String[] rules = proteinSequence.split("(?<=[RK])(?!=[P])");
		
		// Final result object
		List<String> peptides = new ArrayList<>();
		
		// Add all peptide
		for (int i = 0; i < rules.length; i++) {
			if (rules[i].length()>=minLength && rules[i].length()<=maxLength) {
				peptides.add(rules[i]);
			}
		}
		
		// Loop of missed cleavages (mc)
		for (int mc = 1; mc <= missedCleavages; mc++) {
			for (int i = 0; i < rules.length-mc; i++) {
				// Build a concatenated sequences from neighbours
				StringBuilder builder = new StringBuilder(rules[i]);
				for (int j = 1; j <= mc; j++) {
					builder.append(rules[i+j]);
					String newSequence = builder.toString();
					if (newSequence.length()>=minLength &&	newSequence.length()<=maxLength) {
						peptides.add(newSequence);
					}
				}
			}
		}
		return peptides;
	}
}
