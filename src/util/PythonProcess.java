package util;
import java.io.IOException;

public class PythonProcess extends Thread {
	
		///////////////
		// Variables //
		///////////////

		/**
	     * The path for the python script.
	     */
	    protected String scriptPath = null;

	    
		/////////////////
		// Constructor //
		/////////////////
		
		/**
		 * Constructor of python script
		 * @param scriptPath Path to the script
		 * @author Felix Hartkopf
		 */
		public PythonProcess(String scriptPath){
			this.scriptPath = scriptPath;
		}
		
		///////////////////
		// Class Methods //
		///////////////////

		/**
		 *  Method to start PythonProcess task
		 */
		public void run(){
			try {
				Process p;
				p = Runtime.getRuntime().exec("python "+scriptPath);
				System.out.println("Waiting for python with "+scriptPath+" ...");
				p.waitFor();
				java.io.InputStream is=p.getInputStream();
		        byte b[]=new byte[is.available()];
		        is.read(b,0,b.length);
		        System.out.println(new String(b));
			    System.out.println("Python done with "+scriptPath);	
			} 
			catch (IOException | InterruptedException e) {
				e.printStackTrace();

			}
		}
		
			

}
