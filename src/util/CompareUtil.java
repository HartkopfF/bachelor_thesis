package util;

import java.util.Arrays;


public class CompareUtil {


	/**
	 * Calculates the recall of the number of correct amino acids of a provided de novo sequence in comparison to the actual sequence.
	 * @param actualSequence The actual (true) sequence.
	 * @param denovoSequence The de novo sequence.
	 * @return The calculated recall of the de novo sequence. 
	 */
	public static double calculateRecall(String actualSequence, String denovoSequence) {
		int count = 0;
		if (actualSequence.length() == denovoSequence.length()) {
			for (int i = 0; i < actualSequence.length(); i++) {
				if (actualSequence.charAt(i) == denovoSequence.charAt(i)) {
					count++;
				}
			}
		} else if (actualSequence.length() > denovoSequence.length()) {
			for (int i = 0; i < denovoSequence.length(); i++) {
				if (actualSequence.charAt(i) == denovoSequence.charAt(i)) {
					count++;
				}
			}
		} else {
			for (int i = 0; i < actualSequence.length(); i++) {
				if (actualSequence.charAt(i) == denovoSequence.charAt(i)) {
					count++;
				}
			}
		}
		double recall = count * 100.0 / actualSequence.length() * 1.0;
		return recall;
	}

	/**
	 * Calculates the precision of the number of correct amino acids of a provided de novo sequence in comparison to the actual sequence.
	 * @param actualSequence The actual (true) sequence.
	 * @param denovoSequence The de novo sequence.
	 * @return The calculated precision of the de novo sequence. 
	 */
	public static double calculatePrecision(String actualSequence, String denovoSequence) {
		int count = 0;
		if (actualSequence.length() == denovoSequence.length()) {
			for (int i = 0; i < actualSequence.length(); i++) {
				if (actualSequence.charAt(i) == denovoSequence.charAt(i)) {
					count++;
				}
			}
		} else if (actualSequence.length() > denovoSequence.length()) {
			for (int i = 0; i < denovoSequence.length(); i++) {
				if (actualSequence.charAt(i) == denovoSequence.charAt(i)) {
					count++;
				}
			}
		} else {
			for (int i = 0; i < actualSequence.length(); i++) {
				if (actualSequence.charAt(i) == denovoSequence.charAt(i)) {
					count++;
				}
			}
		}
		double recall = count * 100.0 / denovoSequence.length() * 1.0;
		return recall;
	}

	/**
	 * Aligns two sequences.
	 * @param actualSequence
	 * @param denovoSequence
	 * @return
	 */
	public static String[] align(String actualSequence, String denovoSequence) {
		int[][] T = new int[actualSequence.length() + 1][denovoSequence.length() + 1];

		for (int i = 0; i <= actualSequence.length(); i++)
			T[i][0] = i;

		for (int i = 0; i <= denovoSequence.length(); i++)
			T[0][i] = i;

		for (int i = 1; i <= actualSequence.length(); i++) {
			for (int j = 1; j <= denovoSequence.length(); j++) {
				if (actualSequence.charAt(i - 1) == denovoSequence.charAt(j - 1))
					T[i][j] = T[i - 1][j - 1];
				else
					T[i][j] = Math.min(T[i - 1][j], T[i][j - 1]) + 1;
			}
		}

		StringBuilder aa = new StringBuilder(), bb = new StringBuilder();
		StringBuilder cc = new StringBuilder(), dd = new StringBuilder();
		for (int i = actualSequence.length(), j = denovoSequence.length(); i > 0 || j > 0; ) {
			if (i > 0 && T[i][j] == T[i - 1][j] + 1) {
				aa.append(actualSequence.charAt(--i));
				cc.append(actualSequence.charAt(i));
				bb.append("-");
			} else if (j > 0 && T[i][j] == T[i][j - 1] + 1) {
				bb.append(denovoSequence.charAt(--j));
				dd.append(denovoSequence.charAt(j));
				aa.append("-");
			} else if (i > 0 && j > 0 && T[i][j] == T[i - 1][j - 1]) {
				aa.append(actualSequence.charAt(--i));
				bb.append(denovoSequence.charAt(--j));
			}
		}
		return new String[]{aa.reverse().toString(), bb.reverse().toString(), cc.reverse().toString(), dd.reverse().toString()};
	}

	/**
	 * Checks whether to strings are shuffled version to each others.
	 * @param string1
	 * @param string2
	 * @return
	 */
	public static boolean areAnagrams(String string1, String string2) {
		char[] ch1 = string1.toCharArray();
		char[] ch2 = string2.toCharArray();
		Arrays.sort(ch1);
		Arrays.sort(ch2);
		return Arrays.equals(ch1,ch2);
	}

	public static double getSharedSequenceRatio(String string1, String string2) {
		double ratio = 0.0;

		if (string1.length() != string2.length()) {
			System.out.println("String length not equal!");
		}

		int length = string1.length();

		int shared = 0;
		for (int i = 0; i < length; i++) {
			if (string1.charAt(i) == string2.charAt(i)) {
				shared++;
			}
		}
		ratio = shared * 1.0 / length * 1.0;
		return ratio;
	}
}

