package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.List;

import org.mapdb.BTreeMap;

import model.Spectrum;


/**
 * Class to read in a mgf file and to create it into a list of Spectrum objects
 * @author Felix Hartkopf
 *
 */
public class SpectraIO {
	public static BTreeMap<Integer,Spectrum> readMgfFile(String mgfFile,BTreeMap<Integer,Spectrum> spectra) throws FileNotFoundException, IOException{
		System.out.println("\nStarting input of MS/MS spectra...\n");

		// Threadpool
		//int cores = Runtime.getRuntime().availableProcessors();
		//ExecutorService executor = Executors.newFixedThreadPool(cores);

		// Count lines of file
		LineNumberReader  lnr = new LineNumberReader(new FileReader(new File(mgfFile)));
		lnr.skip(Long.MAX_VALUE);
		int lines = lnr.getLineNumber() + 1; //Add 1 because line index starts at 0
		lnr.close();

		// Progress bar
		long progress = 1;
		long startTimeInput = System.currentTimeMillis();

		try(BufferedReader br = new BufferedReader(new FileReader(mgfFile))) {
			String line = br.readLine();
			progress++;

			String title = null;
			double pepmass = 0;
			double pepmassInt = 0;
			int charge = 0;
			double rtinseconds = 0;
			int scans = 0;
			List<Pair<Double,Double>> peaks = new ArrayList<Pair<Double,Double>>();

			while (line != null) {
				if(line.length()>0){
					if(line.substring(0, 5).equals("TITLE")){
						title = line.substring(6);
					}else if(line.length()==0){
					}else if(line.substring(0, 5).equals("PEPMA")){
						pepmass = Double.parseDouble(line.substring(8).split(" ")[0]);
						if(line.substring(8).split(" ").length>1){
							pepmassInt = Double.parseDouble(line.substring(8).split(" ")[1]);
						}else{
							pepmassInt = -1.0;
						}
					}else if(line.substring(0, 5).equals("CHARG")){
						charge = Integer.parseInt(line.substring(7,8));
					}else if(line.substring(0, 5).equals("RTINS")){
						rtinseconds = Double.parseDouble(line.substring(12));
					}else if(line.substring(0, 5).equals("SCANS")){
						scans = Integer.parseInt(line.substring(6));
					}else if(Character.isDigit(line.charAt(0))){
						while(line.length()>0&&(!line.substring(0, 3).equals("END"))){
							String[] tmpString = line.split(" ");
							double mz = (Double.parseDouble(tmpString[0]));
							double intensity = Double.parseDouble(tmpString[1]);
							Pair<Double,Double> pair = new  Pair<Double,Double>(mz,intensity);
							peaks.add(pair);	
							line = br.readLine();
							progress++;
						}
					}

					if(line.length()>0&&line.substring(0, 3).equals("END")){
						Spectrum spectrum = new Spectrum();
						spectrum.title = title;
						spectrum.pepmass = pepmass;
						spectrum.charge = charge;
						spectrum.rtinseconds = rtinseconds;
						spectrum.pepmassInt = pepmassInt;
						if(scans!=0){
							spectrum.scans = scans;
						}else{
							String[] titleSplit = title.split("=");
							String titleSplitSub = titleSplit[titleSplit.length-1];
							spectrum.scans = Integer.parseInt(titleSplitSub.substring(0, titleSplitSub.length()-1));
						}
						spectrum.peaks.addAll(peaks);
						spectra.put(spectrum.scans, spectrum);
						//Runnable worker = new MultiThreadSerializer(primaryMap, scans, spectrum);
						//executor.execute(worker);
						ProgressBar.printProgress(startTimeInput, lines, progress);
					}
				}
				peaks.removeAll(peaks);
				line = br.readLine();
				progress++;
			}	
			//executor.shutdown();
			//while (!executor.isTerminated()) {}
			System.out.println("\nFinished input");
		}
		return spectra;
	}

}
