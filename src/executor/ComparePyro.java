package executor;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import model.NovorHit;
import plots.Histogram;
import util.MSGFplusIO;

public class ComparePyro {

	public static void main(String[] args) throws IOException {
		String modifiedPath = "output/Novor/Pyro_Velos.csv";
		String unmodifiedPath = "output/Novor/Pyro_Velos_unmodified.csv";
		String msgfplusPath = "output/MSGFplus/Pyro_Velos.tsv";
		String mgfPath = "res/spectra/pyro/Pyro_Velos.mgf";
		Map<Integer, NovorHit> modified = NovorHit.readNovorHitsFromFile(modifiedPath,mgfPath);

		Map<Integer, NovorHit> unmodified = NovorHit.readNovorHitsFromFile(unmodifiedPath,mgfPath);

		Map<Integer, String> msgfplus = MSGFplusIO.parseMGSFOutputBA(msgfplusPath,0.01);	
		
		System.out.println("Size MSGFplus: "+msgfplus.size());
		System.out.println("Size Modified: "+modified.size());
		System.out.println("Size Unmodified: "+unmodified.size());

		Set<Integer> scanIDs = modified.keySet();
		Set<Integer> scanIDs2 = unmodified.keySet();
		Set<Integer> scanIDs3 = msgfplus.keySet();


		scanIDs.retainAll(scanIDs2);
		scanIDs2.retainAll(scanIDs);
		


		System.out.println("Size hitRatio: "+scanIDs.size());
		System.out.println("Size MSGFplus: "+msgfplus.size());
		System.out.println("Size Modified: "+modified.size());
		System.out.println("Size Unmodified: "+unmodified.size());


		Map<Integer, Double> hitRatio = new HashMap<Integer, Double>();


		/////////////////////////////
		// Unmodified vs. Modified //
		/////////////////////////////

		for(Integer id : scanIDs) {
			NovorHit mod = modified.get(id);
			NovorHit unmod = unmodified.get(id);

			String modSeq = mod.getDenovoSequence();
			String unmodSeq = unmod.getDenovoSequence();

			char[] first  = modSeq.toLowerCase().toCharArray();
			char[] second = unmodSeq.toLowerCase().toCharArray();
			int counter = 0;
			int counter2 = 0;
			int minLength = Math.min(first.length, second.length);

			for(int i = 0; i < minLength; i++)
			{
				if (first[i] == second[i])
				{
					counter++;    
				}

			}
			if(first.length-second.length>0) {
				int gap = first.length-second.length;
				first  = modSeq.substring(gap).toLowerCase().toCharArray();
				for(int i = 0; i < minLength; i++)
				{
					if (first[i] == second[i])
					{
						counter2++;    
					}

				}
			}else if(second.length-first.length>0){
				int gap = second.length-first.length;
				first  = unmodSeq.substring(gap).toLowerCase().toCharArray();
				for(int i = 0; i < minLength; i++)
				{
					if (first[i] == second[i])
					{
						counter2++;    
					}

				}
			}

			if(counter2 > counter) {
				counter = counter2;
			}
			double ratio = counter/(double)minLength;
			hitRatio.put(id, ratio);
			System.out.println("Modified:   "+modSeq);
			System.out.println("Unmodified: "+unmodSeq);
			System.out.println("Ratio: "+hitRatio.get(id));

		}

		double[] target = new double[hitRatio.size()];
		int i = 0;
		for(Integer key : scanIDs) {
			double value = hitRatio.get(key);
			target[i] = value;
			i++;
		}

		Histogram.createHistogram(target,"Unmodified/MSGFplus", "Recall");

		double mean = 0;

		for(Integer id :scanIDs) {
			mean += hitRatio.get(id);
		}
		mean = mean/(double)hitRatio.size();
		System.out.println("Mean: "+mean);
		
		hitRatio.clear();
		
	

		///////////////////////////
		// Modified vs. MSGFplus //
		///////////////////////////
		modified = NovorHit.readNovorHitsFromFile(modifiedPath,mgfPath);
		msgfplus = MSGFplusIO.parseMGSFOutputBA(msgfplusPath,0.01);
		
		scanIDs = modified.keySet();
		scanIDs2 = unmodified.keySet();
		scanIDs3 = msgfplus.keySet();
		
		scanIDs3.retainAll(scanIDs);
		scanIDs.retainAll(scanIDs3);


		for(Integer id :scanIDs) {
			String msgfplusSeq = msgfplus.get(id);
			NovorHit hit = modified.get(id);

			if(hit!=null&msgfplusSeq!=null) {
				System.out.println(msgfplusSeq);
				String hitSeq = hit.getDenovoSequence(); 

				char[] first  = hitSeq.toLowerCase().toCharArray();
				char[] second = msgfplusSeq.toLowerCase().toCharArray();
				int counter = 0;
				int counter2 = 0;
				int minLength = Math.min(first.length, second.length);

				for( i = 0; i < minLength; i++)
				{
					if (first[i] == second[i])
					{
						counter++;    
					}

				}
				if(first.length-second.length>0) {
					int gap = first.length-second.length;
					first  = hitSeq.substring(gap).toLowerCase().toCharArray();
					for( i = 0; i < minLength; i++)
					{
						if (first[i] == second[i])
						{
							counter2++;    
						}

					}
				}else if(second.length-first.length>0){
					int gap = second.length-first.length;
					first  = msgfplusSeq.substring(gap).toLowerCase().toCharArray();
					for( i = 0; i < minLength; i++)
					{
						if (first[i] == second[i])
						{
							counter2++;    
						}

					}
				}

				if(counter2 > counter) {
					counter = counter2;
				}
				double ratio = counter/(double)minLength;
				hitRatio.put(id, ratio);
				System.out.println("Modified:   "+hitSeq);
				System.out.println("Unmodified: "+msgfplusSeq);
				System.out.println("Ratio: "+hitRatio.get(id));
			}


		}

		double[] target2 = new double[scanIDs.size()];
		i = 0;
		for(Integer key :scanIDs) {
			if(hitRatio.containsKey(key)) {
				double value = hitRatio.get(key);
				target2[i] = value;
				i++;
			}
		}

		Histogram.createHistogram(target2,"Unmodified/MSGFplus", "Recall");

		double mean2 = 0;

		for(Integer key :scanIDs){
			if(hitRatio.containsKey(key)) {
				mean2 += hitRatio.get(key);
			}
		}
		mean2 = mean2/(double)hitRatio.size();
		System.out.println("Mean: "+mean2);
		System.out.println("#modified Novor: "+modified.size());
		System.out.println("#MSGFplus: "+msgfplus.size());

		hitRatio.clear();
		


		
		
		/////////////////////////////
		// Unmodified vs. MSGFplus //
		/////////////////////////////
		
		unmodified = NovorHit.readNovorHitsFromFile(unmodifiedPath,mgfPath);
		msgfplus = MSGFplusIO.parseMGSFOutputBA(msgfplusPath,0.01);	
		
		scanIDs = modified.keySet();
		scanIDs2 = unmodified.keySet();
		scanIDs3 = msgfplus.keySet();
		
		scanIDs2.retainAll(scanIDs3);
		scanIDs3.retainAll(scanIDs2);

		for(Integer id :scanIDs2) {
			String msgfplusSeq = msgfplus.get(id);
			NovorHit hit = unmodified.get(id);

			if(hit!=null&msgfplusSeq!=null) {
				System.out.println(id);
				String hitSeq = hit.getDenovoSequence(); 

				char[] first  = hitSeq.toLowerCase().toCharArray();
				char[] second = msgfplusSeq.toLowerCase().toCharArray();
				int counter = 0;
				int counter2 = 0;
				int minLength = Math.min(first.length, second.length);

				for(i = 0; i < minLength; i++)
				{
					if (first[i] == second[i])
					{
						counter++;    
					}

				}
				if(first.length-second.length>0) {
					int gap = first.length-second.length;
					first  = hitSeq.substring(gap).toLowerCase().toCharArray();
					for(i = 0; i < minLength; i++)
					{
						if (first[i] == second[i])
						{
							counter2++;    
						}

					}
				}else if(second.length-first.length>0){
					int gap = second.length-first.length;
					first  = msgfplusSeq.substring(gap).toLowerCase().toCharArray();
					for(i = 0; i < minLength; i++)
					{
						if (first[i] == second[i])
						{
							counter2++;    
						}

					}
				}

				if(counter2 > counter) {
					counter = counter2;
				}
				double ratio = counter/(double)minLength;
				hitRatio.put(id, ratio);
				System.out.println("Modified:   "+hitSeq);
				System.out.println("Unmodified: "+msgfplusSeq);
				System.out.println("Ratio: "+hitRatio.get(id));
			}


		}

		double[] target3 = new double[scanIDs2.size()];
		i = 0;
		for(Integer key :scanIDs2) {
			if(hitRatio.containsKey(key)) {
				double value = hitRatio.get(key);
				target3[i] = value;
				i++;
			}
		}

		Histogram.createHistogram(target3,"Unmodified/MSGFplus", "Recall");

		double mean3 = 0;

		for(Integer key :scanIDs2){
			if(hitRatio.containsKey(key)) {
				mean3 += hitRatio.get(key);
			}
		}
		mean3 = mean3/(double)hitRatio.size();
		
		
		System.out.println("Size hitRatio: "+hitRatio.size());
		System.out.println("Size MSGFplus: "+msgfplus.size());
		System.out.println("Size Modified: "+modified.size());
		System.out.println("Size Unmodified: "+unmodified.size());

		System.out.println("Mean Unmodified vs. Modified: "+mean);
		System.out.println("Mean Modified vs. MSGFplus: "+mean2);
		System.out.println("Mean Unmodified vs. MSGFplus: "+mean3);
		
		hitRatio.clear();
	}

}

