package executor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.mapdb.BTreeMap;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.Serializer;

import model.PSM;
import model.Spectrum;
import modules.Classifier;
import modules.Features;
import modules.Finalize;
import modules.Synthetic;
import smile.classification.RandomForest;
import util.NovorProcess;
import util.SpectraIO;
import util.StringUtilities;

public class SyntheticMain {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws IOException {


		// start record time
		long startTime = System.currentTimeMillis();

		//////////////////////
		// Read config file //
		//////////////////////

		Properties config = new Properties();
		try {
			Reader configFile = new FileReader("config.config");
			config.load(configFile);
		} catch (IOException e1) {
			System.out.println("Loading config file failed! Please check, if config file is available.");
			e1.printStackTrace();
		}

		// Parameters
		String spectraPath = config.getProperty("spectraPath");
		int nTree = Integer.parseInt(config.getProperty("nTree"));
		boolean balanced = Boolean.parseBoolean(config.getProperty("balanced"));
		String outputFile = config.getProperty("outputFile");
		String novorPath = config.getProperty("Novor");
		String novorParam = config.getProperty("Param");
		double splitRatio = Double.parseDouble(config.getProperty("splitRatio")); 
		String idPath = config.getProperty("idPath");

		//////////////
		// Read IDs //
		//////////////

		List<PSM> psms = Synthetic.readSyntheticIDs(idPath);

		///////////////////
		// Read mgf file //
		///////////////////


		// Init database with MapDB
		File file = new File("spectraDB.db");
		file.delete();
		DB db = DBMaker.heapDB()//fileDB(file)//
				//.fileMmapEnable()
				//.fileMmapPreclearDisable()
				//.allocateStartSize( 1 * 1024*1024*1024)  
				//.allocateIncrement(100 * 1024*1024)      
				.make();

		// Create Heap in DB
		final BTreeMap<Integer,Spectrum> spectra = (BTreeMap<Integer, Spectrum>) db.treeMap("pri").keySerializer(Serializer.INTEGER).create();

		// Read spectra
		try {
			SpectraIO.readMgfFile(spectraPath,spectra);
		} catch (FileNotFoundException e) {
			System.out.println("Features failed! Please check paths in config file.");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Features failed! Please check paths in config file.");
			e.printStackTrace();
		}
		
		////////////////////////////
		// Delete useless spectra //
		////////////////////////////
		
		System.out.println("Start removing useless spectra...");
		Set<Integer> keys = spectra.keySet();
		List<Integer> psmKeys = new ArrayList<Integer>();
		for(PSM psm : psms) {
			psmKeys.add(psm.getScanID());
		}
		for(int key : keys) {
			if(!psmKeys.contains(key)) {
				spectra.remove(key);
			}
		}
		
		System.out.println("Done.");

		
		//////////////////////////
		// Extract training IDs //
		//////////////////////////

		int initSize = psms.size();
		while(psms.size()>initSize*splitRatio) {
			int rando = (int) ((Math.random() * psms.size()));
			psms.remove(rando);
		}

		System.out.println("Initial size: "+initSize);
		System.out.println("Training set size: "+psms.size());
		System.out.println("Test set size: "+(initSize-psms.size()));

		System.out.println("Imported "+spectra.size()+" spectra.");
		
		
		//////////////
		// Features //
		//////////////

		Features features = new Features();
		try {
			features.run(spectra, psms);
		} catch (FileNotFoundException e) {
			System.out.println("Features failed! Please check paths in config file.");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Features failed! Please check paths in config file.");
			e.printStackTrace();
		}

		//////////////////////////
		// Random Forest - Java //
		//////////////////////////

		String trainPath = spectraPath+"_training.mgf";
		String tbcPath = spectraPath+"_tbc.mgf";
		String outputPath = outputFile+"_"+nTree+"_"+balanced+"_";

		RandomForest rf = Classifier.trainRandomForest(trainPath, nTree, balanced);
		Classifier.writeClassesPredict(rf, tbcPath,outputPath);

		System.out.println("\nSummary");

		System.out.println("Error: "+rf.error());

		String[] header = {"ionClass", "charge", "mz", "intensity", "rank", "intBasePeak", "intTotalIonCurrent", "localRank", "intLocalBasePeak", "intLocalTotalIonCurrent", "h2oLoss", "ch4Loss", "nh3Loss", "coLoss", "hLoss", "h2oGain", "ch4Gain", "nh3Gain", "coGain", "hGain", "pCharge", "rtenSec", "pepmass", "pepmassMz", "complIon"};

		double[] importance = rf.importance();
		int h = 0;
		for(double imp : importance) {
			System.out.println(header[h]+" : "+imp);
			h++;
		}
		System.out.println(rf.getTrees());

		////////////////////
		// Removing Noise //
		////////////////////
		String filepath = "output/randomforest/"+outputPath+"classified.mgf";

		String[] predictionFile = spectraPath.split("/");
		for(String string : predictionFile) {
			System.out.println(string);
		}
		System.out.println(filepath);

		try {
			Finalize.removeNoise(filepath);
		} catch (IOException e) {
			System.out.println("Removing noise failed. Please check classified mgf.");
			e.printStackTrace();
		}

		///////////
		// Novor //
		///////////
		System.out.println("Starting de novo sequencing with Novor...");

		System.out.println(filepath+"processed.mgf");

		NovorProcess novor = new NovorProcess(filepath+"processed.mgf", novorPath, outputFile, novorParam);
		novor.run();

		System.out.println("Completed!");
		// debug output
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println("Total time elapsed: "+StringUtilities.millisToShortDHMS(elapsedTime)+" seconds");
	}

}
