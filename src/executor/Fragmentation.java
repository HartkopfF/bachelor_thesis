package executor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.mapdb.BTreeMap;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.Serializer;

import model.Masses;
import model.Peptide;
import model.Spectrum;
import modules.Finalize;
import plots.SpectraPlot;
import util.NovorProcess;
import util.SpectraIO;

public class Fragmentation {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws IOException {

		
		Peptide peptide = new Peptide("AVGIQGNLSGDLLQSGGLLVVSK");
		peptide.fragmentate();

		System.out.println(peptide.aIon2List);
	    System.out.println(peptide.bIon2List);
	    System.out.println(peptide.cIon2List);
	    System.out.println(peptide.xIon2List);
	    System.out.println(peptide.yIon2List);
	    System.out.println(peptide.zIon2List);



	    String spectraPath = "C:/Users/Bready/Downloads/2017_08_13_09_56_07_ms2pip_CID.mgf";

	    // Init database with MapDB
	 		File file = new File("spectraDB.db");
	 		file.delete();
	 		DB db = DBMaker.heapDB()//fileDB(file)//
	 				//.fileMmapEnable()
	 				//.fileMmapPreclearDisable()
	 				//.allocateStartSize( 1 * 1024*1024*1024)  
	 				//.allocateIncrement(100 * 1024*1024)      
	 				.make();

	 		// Create Heap in DB
	 		final BTreeMap<Integer,Spectrum> spectra = (BTreeMap<Integer, Spectrum>) db.treeMap("pri")
	 				.keySerializer(Serializer.INTEGER).create();

	 		// Read spectra
	 		try {
	 			SpectraIO.readMgfFile(spectraPath,spectra);
	 		} catch (FileNotFoundException e) {
	 			System.out.println("Features failed! Please check paths in config file.");
	 			e.printStackTrace();
	 		} catch (IOException e) {
	 			System.out.println("Features failed! Please check paths in config file.");
	 			e.printStackTrace();
	 		}

	 		SpectraPlot.createSpectraPlot(spectra.get(1));
		  
		
		///////////
		// Novor //
		///////////
		System.out.println("Starting de novo sequencing with Novor...");
		String filepath = "res/spectra/UPS1/UPS1_12500amol_R1.mgf";

		System.out.println(filepath+"processed.mgf");

		NovorProcess novor = new NovorProcess(filepath, "C:/Users/hartkopff/PortableProgramms/Novor/lib", "UPS1_12500amol_R1_unmodified", "C:/Users/hartkopff/PortableProgramms/Novor/params.txt");
		novor.run();

		System.out.println("Completed!");



		double weight = 1.01	 ;
		for(int charge = 1; charge<=3;charge++){
			double massShift = ((weight+charge*Masses.Hydrogen)/charge)-1;
			System.out.println(massShift);

			// ga = 65.03661 b	
			// g
		}
		 

		////////////////////
		// Removing Noise //
		////////////////////
		

		System.out.println(filepath);

		try {
			Finalize.removeNoise(filepath);
		} catch (IOException e) {
			System.out.println("Removing noise failed. Please check classified mgf.");
			e.printStackTrace();
		}

		for(int charge = 1; charge <= 3; charge++){
			double massShift = 18.02;
			massShift = ((massShift+charge*Masses.Hydrogen)/charge)-1;
			System.out.println(massShift);
		}
		


	}

}
