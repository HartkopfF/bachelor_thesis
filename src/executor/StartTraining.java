package executor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.mapdb.BTreeMap;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.Serializer;

import model.Spectrum;
import plots.Histogram;
import plots.LinePlots;
import plots.SpectraPlot;
import util.SpectraIO;
import util.StringUtilities;

public class StartTraining {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws FileNotFoundException, IOException {

		// Start record time
		long startTime = System.currentTimeMillis();

		// Init database with MapDB
		File file = new File("spectraDB.db");
		file.delete();
		DB db = DBMaker.heapDB()//fileDB(file)//
				//.fileMmapEnable()
				//.fileMmapPreclearDisable()
				//.allocateStartSize( 1 * 1024*1024*1024)  
				//.allocateIncrement(100 * 1024*1024)      
				.make();

		// Create Heap in DB
		final BTreeMap<Integer,Spectrum> spectra = (BTreeMap<Integer, Spectrum>) db.treeMap("pri")
				.keySerializer(Serializer.INTEGER).create();

		// Read spectra
		String mgfFile = "res/spectra/cpxv/CPXV-0,1MOI-supernatant-HEp-24h.mgf";
		//String mgfFile = "res/spectra/test/testSpectra_small.mgf";
		//String mgfFile = "res/spectra/proteometools/01748a_BG2-TUM_second_pool_49_01_01-2xIT_2xHCD-1h-R1.mgf";
		SpectraIO.readMgfFile(mgfFile,spectra);

		System.out.println("Imported "+spectra.size()+" spectra.");


		// Add MSGFplus results to spectra
		Spectrum.addMSGFplusInfo(spectra, "res/msgfplus/cpxv_crap_allVirus_human_msgfp.tsv");

		// Plot spectra
		SpectraPlot.createSpectraPlot(spectra.firstEntry().getValue());

		
		// Histograms
		Histogram.plotFDR(spectra);
		// XY Plots
		LinePlots.plotFDRtoNumberPeaks(spectra);
		LinePlots.plotFDRtoMeanInt(spectra);
		LinePlots.plotFDRtoTop50Mean(spectra);
		LinePlots.plotFDRtoTop5Mean(spectra);
		LinePlots.plotFDRtoPrecursor(spectra);
		LinePlots.plotFDRtoPrecursorInt(spectra);
		LinePlots.plotFDRtoRtintime(spectra);
		

		// Close database
		db.close();
		System.out.println("\nSpectra database closed!");

		// Elapsed time
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println("Total time elapsed: "+StringUtilities.millisToShortDHMS(elapsedTime)+" seconds");
	}

}
