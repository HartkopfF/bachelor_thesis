
package executor;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import smile.classification.RandomForest;
import smile.data.AttributeDataset;
import smile.data.parser.ArffParser;


public class SparkTutorial{

	public static void main(String[] args) throws FileNotFoundException, IOException, ParseException  {
		
		
		ArffParser arffParser = new ArffParser();
		arffParser.setResponseIndex(4);
		AttributeDataset weather = arffParser.parse(new FileInputStream("res/weka/iris.arff"));
		double[][] x = weather.toArray(new double[weather.size()][]);
		int[] y = weather.toArray(new int[weather.size()]);
		
		
		RandomForest RF = new RandomForest(x,y,200);
		
		System.out.println(RF.error());
		for(int i=0; i < x.length ; i++){
			int result = RF.predict(x[i]);
			System.out.println(result);
		}
		
	}
}