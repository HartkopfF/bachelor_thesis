package executor;

import java.io.IOException;
import modules.Classifier;
import smile.classification.RandomForest;

public class RandomForestExecutor {
	public static void main(String[] args) throws IOException {
		
		String trainPath = args[0];
		String tbcPath = args[1];
		String outputPath = args[2];
		int nTree = Integer.parseInt(args[3]);
		boolean balanced = Boolean.parseBoolean(args[4]);
		outputPath = outputPath+"_"+nTree+"_"+balanced;
		RandomForest rf = Classifier.trainRandomForest(trainPath, nTree, balanced);
		Classifier.classifyPeaks(tbcPath,outputPath, rf);
		
		System.out.println("\nSummary");

		System.out.println("Error: "+rf.error());
		
		String[] header = {"ionClass", "charge", "mz", "intensity", "rank", "intBasePeak", "intTotalIonCurrent", "localRank", "intLocalBasePeak", "intLocalTotalIonCurrent", "h2oLoss", "ch4Loss", "nh3Loss", "coLoss", "hLoss", "h2oGain", "ch4Gain", "nh3Gain", "coGain", "hGain", "pCharge", "rtenSec", "pepmass", "pepmassMz", "complIon"};

		double[] importance = rf.importance();
		int h = 0;
		for(double imp : importance) {
			System.out.println(header[h]+" : "+imp);
			h++;
		}
		System.out.println(rf.getTrees());
	}
}
