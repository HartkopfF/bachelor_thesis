package executor;

import util.NovorProcess;

public class NovorExecutor {

	public static void main(String[] args) {

		String mgfFile = "output/randomforest/01709a_GH2-TUM_first_pool_114_01_01-DDA-1h-R1-CID_200_false_classified.mgfprocessed.mgf";
		String novorPath = "C:/Users/hartkopff/PortableProgramms/Novor/lib";
		String outputFile = "01709a_GH2-TUM_first_pool_114_01_01-DDA-1h-R1-CID";
		String paramsFile = "C:/Users/hartkopff/PortableProgramms/Novor/params.txt";


		NovorProcess novor = new NovorProcess(mgfFile, novorPath, outputFile, paramsFile);
		novor.run();
		/*
		mgfFile = "output/randomforest/UPS1_12500amol_R1._classified_25.mgf";
		outputFile = "UPS1_12500amol_R1._classified_25.mgf";

		NovorProcess novor1 = new NovorProcess(mgfFile, novorPath, outputFile, paramsFile);
		novor1.run();

		mgfFile = "output/randomforest/UPS1_12500amol_R1._classified_50.mgf";
		outputFile = "UPS1_12500amol_R1._classified_50.mgf";

		NovorProcess novor2 = new NovorProcess(mgfFile, novorPath, outputFile, paramsFile);
		novor2.run();
		 */

	}

}