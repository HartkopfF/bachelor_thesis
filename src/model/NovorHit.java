package model;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import model.PSM;
import util.StringUtilities;

public class NovorHit implements DeNovoHit {

	///////////////
	// Variables //
	///////////////

	// Spectrum ID
	private int spectrumId;

	// Spectrum scan number
	private int scanNumber;

	// Retention time
	private double retentionTime;

	// m/z (data)
	private double mz;
	// Charge
	private int charge;

	// De novo peptide mass.
	private double mass;

	// Data - denovo
	private double massError;

	// 1e6*err/(mz*z))
	private double ppm;

	// Novor score
	private double score;

	// Peptide de novo sequence
	private String denovoSequence;

	// Actual Peptide sequence
	private String actualSequence;

	// Amino acid based score
	private String aaScore;

	//////////////////
	// Constructors //
	//////////////////	

	/**
	 * Default constructor.
	 * @param spectrumId
	 * @param scanNumber
	 * @param mz
	 * @param charge
	 * @param mass
	 * @param massError
	 * @param ppm
	 * @param score
	 * @param denovoSequence
	 * @param aaScore
	 */
	public NovorHit(int spectrumId, int scanNumber, double retentionTime, double mz, int charge, double mass, double massError, double ppm, double score, String denovoSequence, String aaScore) {
		this.spectrumId = spectrumId;
		this.scanNumber = scanNumber;
		this.retentionTime = retentionTime;
		this.mz = mz;
		this.charge = charge;
		this.mass = mass;
		this.massError = massError;
		this.ppm = ppm;
		this.score = score;
		// Treat Ile and Leu equally
		this.denovoSequence = denovoSequence; //.replaceAll("(?:16|57)", "");
		// Remove posttranslational modification characters
		this.denovoSequence = this.denovoSequence.replaceAll("\\(O\\)", "");
		this.denovoSequence = this.denovoSequence.replaceAll("\\(Cam\\)", "");
		this.aaScore = aaScore;
	}

	/////////////////////////////
	// Setter & Getter Methods //
	/////////////////////////////


	/**
	 * Returns the spectrum ID.
	 * @return the spectrum ID.
	 */
	public int getSpectrumId() {
		return spectrumId;
	}

	/**
	 * Returns the retention time.	
	 * @return the retention time
	 */
	public double getRetentionTime() {
		return retentionTime;
	}

	public double getMz() {
		return mz;
	}

	public int getCharge() {
		return charge;
	}

	public double getMass() {
		return mass;
	}

	public double getMassError() {
		return massError;
	}

	public double getPpm() {
		return ppm;
	}

	public double getScore() {
		return score;
	}

	public String getSequence() {
		return denovoSequence;
	}

	public String getAaScore() {
		return aaScore;
	}

	public String getDenovoSequence() {
		return denovoSequence;
	}

	public String getActualSequence() {
		return actualSequence;
	}

	public void setActualSequence(String actualSequence) {
		if (actualSequence != null) {
			this.actualSequence = actualSequence.replaceAll("I", "L");
		}
	}

	public int getScanNumber() {
		return scanNumber;
	}

	public void setScanNumber(int scanNumber) {
		this.scanNumber = scanNumber;
	}

	///////////////////
	// Class Methods //
	///////////////////

	/**
	 * Returns true if the sequence contains a PTM.
	 * @return denovoSequence
	 */
	public boolean hasModification() {
		return denovoSequence.contains("(");
	}

	public boolean hasMatch() {
		if (actualSequence == null) return false;
		denovoSequence = denovoSequence.replaceAll("\\(0\\)", "");
		denovoSequence = denovoSequence.replaceAll("\\(1\\)", "");
		return denovoSequence.equalsIgnoreCase(actualSequence);
	}

	public boolean hasPartialMatch(boolean leucineEqualsIsoleucine) {
		if (actualSequence == null) return false;
		denovoSequence = denovoSequence.replaceAll("\\(0\\)", "");
		denovoSequence = denovoSequence.replaceAll("\\(1\\)", "");
		if (leucineEqualsIsoleucine) {
			return actualSequence.replaceAll("I", "L").contains(denovoSequence.replaceAll("I", "L"));
		} else {
			return actualSequence.contains(denovoSequence);
		}
	}

	public String retrieveLCS() {
		denovoSequence = denovoSequence.replaceAll("\\(0\\)", "");
		denovoSequence = denovoSequence.replaceAll("\\(1\\)", "");
		return StringUtilities.longestSubstring(denovoSequence, actualSequence);
	}

	////////////////////
	// Static Methods //
	////////////////////

	/**
	 * Method to read result of novor and create NovorHit objects for every hit
	 * @param novorPath
	 * @param mgfPath
	 * @author Felix Hartkopf
	 * @return hashmap with NovorHit objects. Key is the scan number out of the mgf file
	 */
	public static Map<Integer, NovorHit> readNovorHitsFromFile(String novorPath, String mgfPath){

		// Read Novor file
		Map<Integer, String[]>novorResults = novorReader(novorPath);

		// Read mgf file
		Map<Integer, Integer> idTable = spectraReader(mgfPath);

		// Creating NovorHits
		Map<Integer, NovorHit> allNovorHits = new HashMap<Integer, NovorHit>();
		for (Map.Entry<Integer, Integer> entry : idTable.entrySet()){
			int novorID = entry.getKey();
			int scanID = entry.getValue();
			if(containsKey(novorResults,scanID)){
				NovorHit tmpNovorHit = new NovorHit(novorID, scanID,Double.parseDouble(novorResults.get(scanID)[2]) , Double.parseDouble(novorResults.get(scanID)[3]), Integer.parseInt(novorResults.get(scanID)[4]), Double.parseDouble(novorResults.get(scanID)[5]), Double.parseDouble(novorResults.get(scanID)[6]), Double.parseDouble(novorResults.get(scanID)[7]), Double.parseDouble(novorResults.get(scanID)[8]), novorResults.get(scanID)[9], novorResults.get(scanID)[10]);
				allNovorHits.put(scanID, tmpNovorHit);
			}	
		}

		return allNovorHits;
	}

	/**
	 * Checks if a object with this key exists
	 * @param hashMap
	 * @param key
	 * @author Felix Hartkopf
	 * @return
	 */
	public static boolean containsKey(Map<Integer, String[]> hashMap, int key) {
		return hashMap.get(key) != null;
	}

	/**
	 * Method that reads novor output file
	 * @param novorPath
	 * @return Map filled with novor result and the #ID as key
	 * @author Felix Hartkopf
	 */
	public static Map<Integer, String[]> novorReader(String novorPath){
		String  thisLine = null;
		Map<Integer, String[]> novorResults = new HashMap<Integer, String[]>();

		try{
			BufferedReader br = new BufferedReader(new FileReader(new File(novorPath)));
			while ((thisLine = br.readLine()) != null) {
				if(thisLine.length()>0 && thisLine.charAt(0)!='#'){
					String[] splittedLine = thisLine.split(", ");
					int spectrumKey = Integer.parseInt(splittedLine[1]);
					novorResults.put(spectrumKey,splittedLine);
				}

			}
			br.close();
		}catch(Exception e){
			e.printStackTrace();
		}

		return novorResults;
	}

	/**
	 * Method that reads from a mgf file for the novor input
	 * @param mgfPath
	 * @author Felix Hartkopf
	 */
	public static Map<Integer, Integer> spectraReader(String mgfPath){
		String  thisLine = null;
		Map<Integer, Integer> idTable = new HashMap<Integer, Integer>();
		int novorID = 0;
		try{
			BufferedReader br = new BufferedReader(new FileReader(new File(mgfPath)));
			while ((thisLine = br.readLine()) != null) {
				if(thisLine.length()>0 && thisLine.charAt(0)!='#' && thisLine.substring(0, 5).equals("TITLE")){
					novorID++;
					int scanNumberIndex = thisLine.toLowerCase().indexOf("scan");
					String intValue = ""+novorID;
					if(scanNumberIndex>0) {
						intValue = thisLine.substring(scanNumberIndex,thisLine.length()).replaceAll("[^0-9]", " "); 
					}
					int scanNumber =  Integer.parseInt(Arrays.asList(intValue.trim().split(" ")).get(0)) ;
					idTable.put(novorID, scanNumber);
				}		
			}
			br.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return idTable;
	}

	/**
	 * Method that reads from a mgf file for the novor input
	 * @param NovorHits Map of all Novor hits
	 * @param MSGFplusPSMs Map of all MSGFplus PSMs
	 * @author Felix Hartkopf
	 */
	public static Map<Integer, NovorHit> removeAllIdentified(Map<Integer, NovorHit> NovorHits,TreeMap<String, PSM> MSGFplusPSMs){

		for (Entry<String, PSM> entry : MSGFplusPSMs.entrySet()) {
			PSM object = entry.getValue();
			int tmpScanID = object.getScanID();
			if(NovorHits.containsKey(tmpScanID)){
				NovorHits.remove(tmpScanID);
			}

		}
		return NovorHits;
	}
}
