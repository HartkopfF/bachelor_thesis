package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.mapdb.BTreeMap;

import util.Pair;
import util.ProgressBar;

/**
 * Class for a spectrum. This class holds all information about a spectrum.
 * @author Felix Hartkopf
 *
 */
public class Spectrum implements Serializable {

	///////////////
	// Variables //
	///////////////
	
	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = -5649539045722636748L;

	/**
	 * Title of the spectra.
	 */
	public String title;
	
	/**
	 * Precursor mass of the spectra.
	 */
	public double pepmass;
	
	/**
	 * Precursor mass intensity of the spectra.
	 */
	public double pepmassInt;
	
	/**
	 * Charge state of the precursor.
	 */
	public int charge;
	
	/**
	 * Retention time of the precursor.
	 */
	public double rtinseconds;
	
	/**
	 * Number of the spectra in the MS.
	 */
	public int scans;
	
	/**
	 * This List hold all peaks as pairs with m/z and intensity.
	 */
	public List<Pair<Double,Double>> peaks;
	
	// MSGFplus result below
	
	/**
	 * FDR fetched from MSGFPlus output.
	 */
	public double msgfplusFDR;
	
	/**
	 * The Spectrum ID.
	 */
	public int spectrumIdMSGFplus;	

    /**
     * The peptide sequence.
     */
	public Peptide peptide;
    
    /**
     * The protein accessions.
     */
    public List<String> proteins;

    /**
     * The PSM score.
     */
    public float score;
        
    /**
     * The q-value/fdr of the PSM (optional)
     */
    public float qvalue;
	
    //////////////////
    // Constructors //
    //////////////////
	
	/**
	 * Elementary constructor for the Spectrum class
	 * 
	 * @author Felix Hartkopf
	 */
	public Spectrum(){
		this.peaks = new ArrayList<Pair<Double,Double>>();	
		this.msgfplusFDR = -1.0; // fill value
	}
	
    /////////////////////////////
    // Getter & Setter Methods //
    /////////////////////////////
	
	/**
	 * Getter method for all peaks.
	 * @return List of all peaks as Pair.
	 * @author Felix Hartkopf
	 */
	public List<Pair<Double,Double>> getPeaks(){
		return this.peaks;
	}
	
	public void setMSGFPlusFDR(double fdr){
		this.msgfplusFDR = fdr;
	}
	
	///////////////////
	// Class Methods //
	///////////////////
	
	public static void addMSGFplusInfo(BTreeMap<Integer,Spectrum> spectra, String path) throws FileNotFoundException, IOException{
		
		System.out.println("\nAdding MSGFPlus info to spectra... \n");

		// Count lines of file
		LineNumberReader  lnr = new LineNumberReader(new FileReader(new File(path)));
		lnr.skip(Long.MAX_VALUE);
		int lines = lnr.getLineNumber() + 1; //Add 1 because line index starts at 0
		lnr.close();

		String msgfplusFile = path;
		long progress = 0;
		long startTime = System.currentTimeMillis();
		
		try(BufferedReader br = new BufferedReader(new FileReader(msgfplusFile))) {
			progress++;
			String line = br.readLine(); 	// header
			progress++;
			line = br.readLine(); 			// first data entry
			
			while (line != null) {
				String[] row = line.split("\t");
				int key = Integer.parseInt(row[2]);
				Spectrum spectrum = spectra.get(key);
				if(spectrum!=null){
					double fdr =  Double.parseDouble(row[15]);
					if(spectrum.msgfplusFDR>fdr||spectrum.msgfplusFDR==-1.0){
						spectrum.msgfplusFDR = fdr;
						spectra.replace(key, spectrum);
						ProgressBar.printProgress(startTime, lines, progress);
					}
					spectrum.msgfplusFDR = fdr;
					spectra.replace(key, spectrum);
					
				}
				progress++;
				line = br.readLine(); 			// first data entry

			}
			ProgressBar.printProgress(startTime, lines, progress);

		}
		
	}

	
	
}
